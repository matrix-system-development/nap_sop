@extends('Layouts.master')
@section('title') {{'SOP - '.$files->title}} @endsection
@section('content')
<style>
.float-container {
    position: fixed;
    top: 33%;
    right: 0;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    flex-direction: column;
    width: auto;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    -webkit-box-align: end;
    -ms-flex-align: end;
    align-items: flex-end;
}

.float-container a {
    z-index: 99;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    width: 50px;
    height: 50px;
    /* margin-right: -190px; */
    margin-bottom: 10px;
    padding: 10px 20px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    text-decoration: none;
    color: white;
    border-color: #46b8da;
    border-radius: 5px 0 0 5px;
    background-color: #eb690b;
    -webkit-box-shadow: 0 2px 4px #7d7d7d;
    box-shadow: 0 2px 4px #7d7d7d;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: start;
    -ms-flex-pack: start;
    justify-content: flex-start;
    font-family: sans-serif;
}

/* .float-container a:hover {
    margin-right: 0;
    background-color: #c45100;
    -webkit-box-shadow: 0 2px 4px #7d7d7d;
    box-shadow: 0 2px 4px #7d7d7d;
} */


/* Media queries */
@media screen and (max-width:440px)
{
	.float-container .icon:last-child {
		display: none;
	}
    .float-container
    {
        position: fixed;
        top: auto;
        bottom: 0;

        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
            -ms-flex-direction: row;
                flex-direction: row;

        width: 100%;

        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: auto;
        -webkit-box-align: auto;
        -ms-flex-align: auto;
        align-items: auto;
    }
		.float-container a.icon
    {
        right: 0;
        bottom: 0;

        width: 100%;
        margin-right: 0;
        margin-bottom: 0;
        padding: 5px;

        border-radius: 0;
        -webkit-box-shadow: 0 0 0 #7d7d7d;
                box-shadow: 0 0 0 #7d7d7d;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
		border-left: 1px solid darkorange;
		border-right: 1px solid darkorange;
    }
}

</style>
<div class="container">
  <section class="pb-2 wow fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-cascade wider reverse">
          <div class="card-body card-body-cascade text-center">
            <h2><a><strong>{{$files->title}}</strong></a></h2>
            <p><a href="" style="font-weight: bold; color:black;">{{$files->dept_name}}</a> - {{$files->created_at->isoFormat('dddd, D MMMM Y')}}</p>
          </div>
        </div>
        <hr>
        <div class="excerpt wow fadeIn" data-wow-delay="1.0s">
          <section class="content">
            <div class="container-">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                        <div class="row">
                          <div class="col align-self-start">
                            <button id="prev" class="btn btn-block btn-primary" type="button">Previous</button>
                          </div>
                          <div class="col-lg-3">
                            <button class="btn btn-block btn-primary" id="zoom_in">Zoom In</button>
                          </div>
                          <div class="col-lg-3">
                            <button class="btn btn-block btn-primary" id="zoom_out">Zoom Out</button>
                          </div>
                          <div class="col align-self-end">
                            <button id="next" class="btn btn-block btn-primary" type="button">Next</button>
                          </div>
                        </div>
                        <div class="row" style="font-weight:bold;">
                          <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
                        </div>
                      <div class="container-fluid">
                        <section class="row">
                          <div style="height:850px;overflow-y:scroll;margin: auto;
                          border: 3px solid green;
                          padding: 10px;">
                            <canvas id="the-canvas"></canvas>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                    <!-- /.card -->
                </div>
              </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
          </section>
        </div>

      </div>
    </div>

  </section>

  <div class="float-container">
    <a href="#" id="prev" onclick="onPrevPage()"><</a>
    <a href="#" id="next" onclick="onNextPage()">></a>
    <a href="#" id="zoom_in" onclick="zoomIn()">+</a>
    <a href="#" id="zoom_out" onclick="zoomOut()">-</a>
  </div>


</div>
<script id="url" data-name="{{url('/view/isolate/'.$files->filename.'/'.$additional)}}" page-number="{{$page}}">
  var url = document.getElementById("url").getAttribute("data-name");
  var pagess = document.getElementById("url").getAttribute("page-number");
  var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.10.377/pdf.worker.js';

var pdfDoc = null,
    pageNum =  parseInt(pagess),
    pageRendering = true,
    pageNumPending = null,
    scale = 1.0,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
  pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
    var viewport = page.getViewport({scale: scale});
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);

    // Wait for rendering to finish
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending !== null) {
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });

  // Update page counters
  document.getElementById('page_num').textContent = num;
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
  if (pageRendering) {
    pageNumPending = num;
  } else {
    renderPage(num);
  }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
  if (pageNum <= 1) {
    return;
  }
  pageNum--;
  queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
  if (pageNum >= pdfDoc.numPages) {
    return;
  }
  pageNum++;
  queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

function zoomIn() {
  if(scale >= 2.5)
  {
    return;
  }
  scale +=  0.1;
  console.log(scale);
  queueRenderPage(pageNum);
}
document.getElementById('zoom_in').addEventListener('click', zoomIn);

function zoomOut() {
  if(scale <= 1)
  {
    return;
  }
  scale -=  0.1;
  queueRenderPage(pageNum);
}
document.getElementById('zoom_out').addEventListener('click', zoomOut);
/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
  pdfDoc = pdfDoc_;
  document.getElementById('page_count').textContent = pdfDoc.numPages;

  // Initial/first page rendering
  renderPage(pageNum);
});
</script>
@endsection
