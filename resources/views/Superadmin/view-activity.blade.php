@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              {{-- <h1 class="m-0">Activity Log</h1> --}}
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card card-cascade narrower z-depth-1">
                <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                  <a href="" class="white-text mx-3">Activity Logs</a>
                </div>
                <div class="px-4">
                  <div class="table-responsive">
                    <!-- Table -->
                    <table class="table table-hover mb-0">
                      <thead>
                        <tr>
                          <th class="th-lg"><a>Name<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">Activity<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">Browser<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">IP Address<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">Time<i class="fas fa-sort ml-1"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ( $activity_log as $act)     
                        @php
                          $parse = json_decode($act->properties, true);
                        @endphp               
                        <tr>
                          <td>{{$act->user->name}}</td>
                          <td>{{$act->description}}</td>
                          <td>
                            {{$parse['browser']}}
                          </td>
                          <td>
                            {{$parse['ip']}}
                          </td>
                          <td>{{$act->created_at}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                      <!-- Table body -->
                    </table>
                    <!-- Table -->
                  </div>
      
                  <hr class="my-0">
      
                  <!-- Bottom Table UI -->
                  <div class="d-flex justify-content-between">
                    <nav class="my-4">
                      <ul class="pagination pagination-circle pg-blue mb-0">
                        <div class="d-flex justify-content-center">
                          {{ $activity_log->links() }}
                        </div>
                        <li class="page-item clearfix d-none d-md-block"><a class="page-link waves-effect waves-effect">Last</a></li>
                      </ul>
                    </nav>
                    <!-- /Pagination -->
      
                  </div>
                  <!-- Bottom Table UI -->
      
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
@livewireScripts

<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

