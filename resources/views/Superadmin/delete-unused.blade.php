@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Clear Unused File</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="col-md-12">
                    <div class="text-center">
                      @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                      @elseif(session()->has('error'))
                        <div class="alert alert-danger">
                          {{ session()->get('error') }}
                        </div>
                      @endif
                      <a href="" class="btn btn-danger btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#deleteFile">Delete Unused File <i class="far fa-eye ml-1"></i></a>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    
        <!-- Central Modal Danger Demo -->
        <div class="modal fade" id="deleteFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
          aria-hidden="true">
          <div class="modal-dialog modal-notify modal-danger" role="document">
            <!-- Content -->
            <div class="modal-content">
              <!-- Header -->
              <div class="modal-header">
                <p class="heading">Confirmation</p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
              </div>

              <!-- Body -->
              <div class="modal-body">

                <div class="row">
                  <div class="col-3">
                    <p></p>
                    <p class="text-center"><i class="fas fa-exclamation-triangle fa-4x"></i></p>
                  </div>

                  <div class="col-9">
                    <p>This Process will syncronize storage folder with database, are you sure?</p>
                  </div>
                </div>
              </div>

              <!-- Footer -->
              <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-danger" href="{{route('delete.unused.process')}}">Yes</i></a>
                <a type="button" class="btn btn-outline-danger waves-effect" data-dismiss="modal">No</a>
              </div>
            </div>
            <!-- Content -->
          </div>
        </div>
    </section>
</div>
@livewireScripts

<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

