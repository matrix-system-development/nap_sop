@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0">History</h1>
            </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
  <section class="content">
    <div class="container-fluid">
      <!-- Timelime example  -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <div class="timeline">
            <livewire:components.history.history-posts />
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@livewireScripts
@endsection