@extends('Layouts.master')
@section('title') {{'SOP'}} @endsection
@section('content')
<main class="mt-0 pt-0">

  <section class="section mb-5 pb-4">
    <div class="row">
      <!-- Grid column -->
      <div class="col-12">
        <ul class="nav nav-tabs md-tabs" role="tablist" style="background-color: #151A48 !important; ">
          @foreach ($depts as $dept)
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel{{$dept->id}}"
              role="tab">{{$dept->dept_name}}</a>
          </li>
          @endforeach
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
              aria-expanded="false">Others</a>
            <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
              @foreach ($others as $oth)
              <a class="dropdown-item" data-toggle="tab" href="#panel{{$oth->id}}"
                role="tab">{{$oth->dept_name}}</a>
              @endforeach
            </div>
          </li>
        </ul>
        <div class="tab-content card">
          @foreach ($depts as $dept)
            <div class="tab-pane fade" id="panel{{$dept->id}}" role="tabpanel">
                @livewire('sop-paginate-by-dept', [
                  'selected_id' => $dept->id,
                  ],
                  key($dept->code)
                )
            </div>
          @endforeach
          @foreach ($others as $oth)
          <div class="tab-pane fade" id="panel{{$oth->id}}" role="tabpanel">
            @livewire('sop-paginate-by-dept', [
              'selected_id' => $oth->id,
              ],
              key($oth->code)
            )
          </div>
          @endforeach
        </div>
      </div>
    </div>
    {{-- <div class="row">
      <div class="col-12 my-3">

      </div>
    </div> --}}

</main>
@endsection