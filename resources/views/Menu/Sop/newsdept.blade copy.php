@extends('Layouts.master')
@section('title') {{'SOP'}} @endsection
@section('content')
<style>
  .pdf-thumbnail {
    position: relative;
    border-radius: 2px; 
    box-shadow: 0 2px 5px rgb(0 0 0 / 0.2); 
    background-color: #f5f4f4;
}

.pdf-thumbnail:after {
    content: "";
    display: block;
    padding-bottom: 100%;
}

.pdf-thumbnail img {
    position: absolute; /* Take your picture out of the flow */
    top: 0;
    bottom: 0;
    left: 0;
    right: 0; /* Make the picture taking the size of it's parent */
    width: 100%; /* This if for the object-fit */
    height: 100%; /* This if for the object-fit */
    object-fit: cover; /* Equivalent of the background-size: cover; of a background-image */
    object-position: center;
}
.main {
    /* padding-left: 2rem;
    padding-right: 2rem; */
  }

  .b-0 {
    bottom: 0;
}
.bg-shadow {
    background: rgba(76, 76, 76, 0);
    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(179, 171, 171, 0)), color-stop(49%, rgba(48, 48, 48, 0.37)), color-stop(100%, rgba(19, 19, 19, 0.8)));
    background: linear-gradient(to bottom, rgba(179, 171, 171, 0) 0%, rgba(48, 48, 48, 0.71) 49%, rgba(19, 19, 19, 0.8) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c4c4c', endColorstr='#131313', GradientType=0 );
}
.top-indicator {
    right: 0;
    top: 1rem;
    bottom: inherit;
    left: inherit;
    margin-right: 1rem;
}
.overflow {
    position: relative;
    overflow: hidden;
}
.zoom img {
    transition: all 0.2s linear;
}
.zoom:hover img {
    -webkit-transform: scale(1.4);
    transform: scale(1.4);
}
</style>
<main class="mt-0 pt-0">
  <section class="section mb-5 pb-5">
    <div class="row">
      <div class="col-12 my-3">
        <div class="classic-tabs">
          <div class="tabs-wrapper">
            <ul class="nav tabs-blue" role="tablist">
              @php
              $i = 0;
              @endphp
              @foreach ($depts as $dept)
              @if ($i==0)
              <li class="nav-item">
                <a class="nav-link waves-light active waves-effect waves-light" data-toggle="tab" href="#panel{{$dept->id}}"
                  role="tab">{{$dept->dept_name}}</a>
              </li>
              @else
              <li class="nav-item">
                <a class="nav-link waves-light waves-effect waves-light" data-toggle="tab" href="#panel{{$dept->id}}"
                  role="tab">{{$dept->dept_name}}</a>
              </li>
              @endif

              @php
                $i++;
              @endphp
              @endforeach
            </ul>
          </div>
          <div class="tab-content card">
            @php
              $j = 0;
            @endphp
            @foreach ($depts as $dept)
            @if ($j==0)
              <div class="tab-pane fade in show active" id="panel{{$dept->id}}" role="tabpanel">
                {{-- <livewire:sop-paginate-by-dept :selected_id="$dept->id" :wire:key="$dept->code"/> --}}
                  @livewire('sop-paginate-by-dept', [
                    'selected_id' => $dept->id,
                    ],
                    key($dept->code)
                  )
              </div>
            @else
              <div class="tab-pane fade" id="panel{{$dept->id}}" role="tabpanel">
                  @livewire('sop-paginate-by-dept', [
                    'selected_id' => $dept->id,
                    ],
                    key($dept->code)
                  )
              </div>
            @endif
            @php
              $j++;
            @endphp
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<script>
window.addEventListener('contentChanged', event => {
var createPDFThumbnails = function(){

var worker = null;
var loaded = false;
var renderQueue = [];

// select all img elements with data-pdf-thumbnail-file attribute
var nodesArray = Array.prototype.slice.call(document.querySelectorAll('img[data-pdf-thumbnail-file]'));

if (!nodesArray.length) {
    // No PDF found, don't load PDF.js
    return;
}

if (!loaded && typeof(pdfjsLib) === 'undefined') {
    var src = document.querySelector('script[data-pdfjs-src]').getAttribute('data-pdfjs-src');

    if (!src) {
        throw Error('PDF.js URL not set in "data-pdfjs-src" attribute: cannot load PDF.js');
    }

    var script = document.createElement('script');
    script.src = src;
    document.head.appendChild(script).onload = renderThumbnails;
    loaded = true;
}
else {
    renderThumbnails();
}

function renderThumbnails() {
    if (!pdfjsLib) {
        throw Error("pdf.js failed to load. Check data-pdfjs-src attribute.");
    }

    nodesArray.forEach(function(element) {
        if (null === worker) {
            worker = new pdfjsLib.PDFWorker();
        }

        var filePath = element.getAttribute('data-pdf-thumbnail-file');
        var imgWidth = element.getAttribute('data-pdf-thumbnail-width');
        var imgHeight = element.getAttribute('data-pdf-thumbnail-height');

        pdfjsLib.getDocument({url: filePath, worker: worker}).promise.then(function (pdf) {
            pdf.getPage(1).then(function (page) {
                var canvas = document.createElement("canvas");
                var viewport = page.getViewport({scale: 1.0});
                var context = canvas.getContext('2d');

                if (imgWidth) {
                    viewport = page.getViewport({scale: imgWidth / viewport.width});
                } else if (imgHeight) {
                    viewport = page.getViewport({scale: imgHeight / viewport.height});
                }

                canvas.height = viewport.height;
                canvas.width = viewport.width;

                page.render({
                    canvasContext: context,
                    viewport: viewport
                }).promise.then(function () {
                    element.src = canvas.toDataURL();
                });
            }).catch(function() {
                console.log("pdfThumbnails error: could not open page 1 of document " + filePath + ". Not a pdf ?");
            });
        }).catch(function() {
            console.log("pdfThumbnails error: could not find or open document " + filePath + ". Not a pdf ?");
        });
    });
}
};

if (
document.readyState === "complete" ||
(document.readyState !== "loading" && !document.documentElement.doScroll)
) {
createPDFThumbnails();
} else {
document.addEventListener("DOMContentLoaded", createPDFThumbnails);
}

  });
</script>
<script src="{{ asset('plugins/pdfThumbnails/pdfThumbnails.js') }}"></script>
@endsection