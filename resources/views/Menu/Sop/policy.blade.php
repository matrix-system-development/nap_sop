@extends('Layouts.master')
@section('title') {{'SOP'}} @endsection
@section('content')
<main class="mt-0 pt-0">
  <section class="section mb-5 pb-4 pr-5 pl-5">
    <div class="row">
      <!-- Grid column -->
      <div class="col-12 ">
        @livewire('policy-paginate')
      </div>
    </div>
  <section>
</main>
@endsection