@extends('Layouts.home')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Home</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">SOP Lists</h3>
            </div>
            <div class="card-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <h2 class="card-title">Filter</h2>
                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse" >
                            <i class="fas fa-minus"></i>
                            </button>
                          </div>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-4">
                            <label for="filter-dept"> Filter Based on Department : </label>
                            <select name="filter_dept" id="filter_dept" class="form-control">
                                <option value="" selected default>ALL</option>
                                @foreach($dept as $d)
                                  <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="col-4 offset-4">
                            <label for="filter_search"> Search : </label>
                            <input id="filter_search" type="text" name="filter_search" class="form-control filter_search" placeholder="Search..">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @include('Layouts.Tools._datatables')
              @yield('datatables-html')
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@yield('datatables-script')
@endsection




