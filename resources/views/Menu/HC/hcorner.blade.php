@extends('Layouts.master')
@section('title') {{'HC Corners'}} @endsection
@section('content')
<style>
    .section .row{
    margin-top: 5%;
    margin-bottom: 5%;
}
.section .row .col-md-6{
    background: #f5f5f5;
    margin-right: -2%;
    padding: 3%;
}
.section h3{
    color: #004085;
}
.section p{
    margin-top: 10%;
    color: #545b62;
}
.section img
{
    object-fit: cover;
  object-position: center;
  height: 55vh;
  overflow: hidden;
    width: 100%;
}
</style>
<link rel="stylesheet" href="{{asset('diy/css/announcement.css') }}">
<main class="mt-0 pt-0">
    <section class="section mb-5 pb-5">
        <div class="container">
                @livewire('components.hc.paginate-corner')         
        </div>
    </section>
</main>

@endsection