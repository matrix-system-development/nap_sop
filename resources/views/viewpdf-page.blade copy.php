@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">{{$files->title}}</h1>
          </div>
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body" style="display: block;">
                <div>
                  <div class="row" style="overflow: auto;">
                    <div class="col-6">
                      <button id="prev" class="btn btn-block btn-primary" type="button">Previous</button>
                    </div>
                    <div class="col-6">
                      <button id="next" class="btn btn-block btn-primary" type="button">Next</button>
                    </div>
                  </div>
                  <div style="font-weight:bold;">
                    <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
                  </div>
                </div>
                <div>
                  <canvas id="the-canvas" style="border: 1px solid black; direction: ltr; width: 100%;"></canvas>
                </div>
              </div>
          <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
                Footer
              </div>
          <!-- /.card-footer-->
            </div>
              <!-- /.card -->
          </div>
        </div>
          <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
{{-- <script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script>
<script id="worker" src="{{ asset('plugins/pdf.js/pdf.worker.js') }}"></script>
<script id="url" data-name="{{url($path->file_path)}}" page-number="{{$page}}" type="text/javascript" src="{{ url('/diy/js/viewpdf-page.js') }}"></script> --}}

<script id="url" data-name="{{url('/view/isolate/'.$files->filename)}}" page-number="{{$page}}">
  var url = document.getElementById("url").getAttribute("data-name");
  var pagess = document.getElementById("url").getAttribute("page-number");
  var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.10.377/pdf.worker.js';

var pdfDoc = null,
    pageNum = parseInt(pagess),
    pageRendering = true,
    pageNumPending = null,
    scale = 2.0,
    zoom = 1.0,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
  pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
    var viewport = page.getViewport({scale: scale});
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);

    // Wait for rendering to finish
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending !== null) {
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });

  // Update page counters
  document.getElementById('page_num').textContent = num;
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
  if (pageRendering) {
    pageNumPending = num;
  } else {
    renderPage(num);
  }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
  if (pageNum <= 1) {
    return;
  }
  pageNum--;
  queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
  if (pageNum >= pdfDoc.numPages) {
    return;
  }
  pageNum++;
  queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
  pdfDoc = pdfDoc_;
  document.getElementById('page_count').textContent = pdfDoc.numPages;

  // Initial/first page rendering
  renderPage(pageNum);
});
</script>

@endsection