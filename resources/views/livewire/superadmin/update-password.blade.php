<div>
    @if (session()->has('success'))
    <div class="col-8">
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    </div>
    <script>
        toastr.success({{ session('success') }})
    </script>
    @endif
    <form wire:submit.prevent="submit" enctype="multipart/form-data">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">User Information</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label for="Name">Email</label>
                                    <div class="relative">
                                        <input
                                            type="text"
                                            class="relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                            placeholder="Search User..."
                                            wire:model="query"
                                            wire:click="reset"
                                            wire:keydown.escape="hideDropdown"
                                            wire:keydown.tab="hideDropdown"
                                            wire:keydown.Arrow-Up="decrementHighlight"
                                            wire:keydown.Arrow-Down="incrementHighlight"
                                            wire:keydown.enter.prevent="selectAccount"
                                        />
                                
                                        <input type="hidden" name="account" id="account" wire:model="selectedAccountID">
                                
                                        @if ($selectedAccount)
                                            <a class="absolute cursor-pointer top-2 right-2 text-gray-500" wire:click="reset">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </a>
                                        @endif
                                    </div>
                                    @if(!empty($query) && $selectedAccount == 0 && $showDropdown)
                                    <div class="absolute z-10 bg-white mt-1 w-full border border-gray-300 rounded-md shadow-lg">
                                        @if (!empty($accounts))
                                            @foreach($accounts as $i => $account)
                                                <a
                                                    wire:click="selectAccount({{ $i }})"
                                                    class="block py-1 px-2 text-sm cursor-pointer hover:bg-blue-50 {{ $highlightIndex === $i ? 'bg-blue-50' : '' }}"
                                                >{{ $account['email'] }}</a>
                                            @endforeach
                                        @else
                                            <span class="block py-1 px-2 text-sm">No results!</span>
                                        @endif
                                    </div>
                                @endif
                                
                                    {{-- <input type="email" class="form-control" id="email"wire:model.defer="email" placeholder="Enter Mail"> --}}
                                    {{-- @error('email') <span class="error"  style="color: red;">{{ $message }}</span> @enderror --}}
                                </div>
                                <div class="col-4">
                                    <label for="Name">Password</label>
                                    <input type="password" class="form-control" id="password" wire:model.defer="password" placeholder="Enter Password">
                                    @error('password') <span class="error"  style="color: red;">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
