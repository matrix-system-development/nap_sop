<div>
    @if (session()->has('success'))
    <div class="col-8">
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    </div>
    <script>
        toastr.success({{ session('success') }})
    </script>
    @endif
    <form wire:submit.prevent="submit" enctype="multipart/form-data">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">User Information</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" id="name" wire:model.defer="name" placeholder="Enter Name">
                                @error('name') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label for="Name">Email</label>
                                    <input type="email" class="form-control" id="email"wire:model.defer="email" placeholder="Enter Mail">
                                    @error('email') <span class="error"  style="color: red;">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-4">
                                    <label for="Name">Password</label>
                                    <input type="password" class="form-control" id="password" wire:model.defer="password" placeholder="Enter Password">
                                    @error('password') <span class="error"  style="color: red;">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">User Privilage</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <label for="dept">Department</label>
                                    <select class="form-control" name="dept" wire:model="inputDept" id="" required>
                                        <option value="" hidden>Select a dept</option>
                                        @foreach ($depts as $dept)
                                            <option value="{{$dept->dept_name}}">{{$dept->dept_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('inputDept') <span class="error"  style="color: red;">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-6">
                                    <label for="dept">Role</label>
                                    <select class="form-control" name="role" wire:model="inputRole" id="">
                                        <option value="" hidden>Select a role</option>
                                        @foreach ($roles as $role)
                                            <option value="{{$role->name}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('inputRole') <span class="error"  style="color: red;">{{ $message }}</span> @enderror
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
