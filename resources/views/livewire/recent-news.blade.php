<div>
    <hr class="mb-5 mt-4">

    <section class="mb-5 wow fadeIn" data-wow-delay="0.3s">
        <h3 class="text-center my-5 text-center h3">Recent posts</h3>
        <div class="row">
            @foreach ($news as $item)
            <div class="col-lg-4 col-md-12 mb-4">
                <div class="card">
                  <div class="view overlay">
                    <img src="{{url($item->thumbnail_path)}}" class="card-img-top img-thumbnail img-fluid"
                      alt="" style="    object-fit: cover;
                      object-position: center;
                      height: 35vh;
                      overflow: hidden;">
                    <a href="{{route('news.title', $item->seo_url)}}"> 
                      <div class="mask"></div>
                    </a>
                  </div>
                  <div class="card-body">
                    <!-- Social shares button -->
                    <a class="activator mr-3" href="{{route('news.title', $item->seo_url)}}"><i class="fas fa-share-alt"></i></a>
                    <h5 class="card-title">{{$item->title}}</h5>
                    <hr>
                    <a class="link-text" href="{{route('news.title', $item->seo_url)}}">
                      <h6>Read more <i class="fas fa-chevron-right"></i></h6>
                    </a>
                  </div>
                </div>
            </div>
            @endforeach

        </div>
        <!-- Grid row -->

      </section>
</div>
`