<div>
     <div class="row">
        <div class="col-12">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Preview</h3>
                    </div>
                    <div class="card-body">
                        @if($convertedFile)
                            
                            <iframe src="{{ $convertedFile }}" style="width: 100%;height: 800px;border: none;"></iframe>
                            <div x-data="{ convertedname: @entangle('convertedname') }">
                                <input type="hidden" id="convertedname" x-model="convertedname">
                            </div>
                        @else
                            <iframe src="{{ $previewFile->temporaryUrl() }}" style="width: 100%;height: 800px;border: none;"></iframe>
                        @endif
 
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div> 
