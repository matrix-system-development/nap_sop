<div>
    {{-- <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="input-group" x-data="{isTyped: false}"> 
                <input class="form-control form-control-lg"
                type="search"
                name="search"
                id="search"
                x-ref="searchField"
                x-on:input="isTyped = ($event.target.value != '')"
                placeholder='Search... '
                autocomplete="off"
                wire:model="search"
                oninput="resetSearch()"
                x-on:keydown.window.prevent.slash="$refs.searchField.focus()"
                x-on:keyup.escape="isTyped = false; $refs.searchField.blur()">

                <div class="input-group-append">
                    <button wire:click="resetFilters()" type="submit" class="btn btn-lg btn-default">
                        <i class="fa fa-redo"></i>
                    </button>
                </div>
            </div>
        </div> 
    </div> --}}
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="input-group"> 
                <input class="form-control form-control-lg"
                type="search"
                name="searchModal"
                id="searchModal"
                data-toggle="modal" data-target="#searchResult"
                placeholder='Search... '
                autocomplete="off" readonly="readonly">
                <div class="input-group-append">
                    <button wire:click="resetFilters()" type="submit" class="btn btn-lg btn-default">
                        <i class="fa fa-redo"></i>
                    </button>
                </div> 
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="card-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h2 class="card-title">Search Results</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse" >
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                @if($tables->isEmpty())
                                    <div class="alert alert-warning">
                                        Your query returned zero results.
                                    </div>
                                @else
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table table-bordered table-hover data-table overflow-auto">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <a wire:click.prevent="sortBy('title')" role="button" href="#">
                                                            Title
                                                            @include('Layouts.Includes.sort-icon', ['field' => 'title'])
                                                        </a>
                                                    </th>
                                                    <th>
                                                        <a wire:click.prevent="sortBy('dept_name')" role="button" href="#">
                                                            Department Name
                                                            @include('Layouts.Includes.sort-icon', ['field' => 'dept_name'])
                                                        </a>
                                                    </th>
                                                    <th>
                                                        <a wire:click.prevent="sortBy('status')" role="button" href="#">
                                                            status
                                                            @include('Layouts.Includes.sort-icon', ['field' => 'status'])
                                                        </a>
                                                    </th>
                                                    <th>
                                                        <a wire:click.prevent="sortBy('created_at')" role="button" href="#">
                                                            Created at
                                                            @include('Layouts.Includes.sort-icon', ['field' => 'created_at'])
                                                        </a>
                                                    </th>
                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($tables as $tbl)
                                                    <tr>
                                                        <td>{{ $tbl->title }}</td>
                                                        <td>{{ $tbl->dept_name }}</td>
                                                        <td>{{ $tbl->status }}</td>
                                                        <td>{{ $tbl->created_at->format('m-d-Y') }}</td>
                                                        <td><a href='{{route("view.id",$tbl->id_post)}}' class='edit btn btn-primary btn-sm' target='_blank'>View</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        {{ $tables->links() }}
                                    </div>
                                </div>
                                @endif
                                @if(!empty($search))
                                <div class="row">
                                    @foreach($contents as $ctn)
                                    <div id="accordion" class="col-12">
                                        <div class="card card-primary card-outline">                                            
                                            <a class="d-block w-100" >
                                                <div class="card-header">
                                                    <h4 class="card-title w-100">
                                                        {{ $ctn->title }} - Page {{ $ctn->page }}
                                                    </h4>
                                                </div>
                                            </a>
                                            <div data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    {{ $ctn->content }}
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>  
                                    @endforeach                                 
                                </div>
                                @else
                                {{$isEmpty}}
                                @endif 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="searchResult">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#17a2b8;">
                    <div class="input-group" x-data="{isTyped: false}"> 
                        <input class="form-control form-control-lg"
                        type="search"
                        name="search"
                        id="search"
                        x-ref="searchField"
                        x-on:input="isTyped = ($event.target.value != '')"
                        placeholder='Search... '
                        autocomplete="off"
                        wire:model="search"
                        oninput="resetSearch()"
                        x-on:keydown.window.prevent.slash="$refs.searchField.focus()"
                        x-on:keyup.escape="isTyped = false; $refs.searchField.blur()">

                        <div class="input-group-append">
                            <button wire:click="resetFilters()" type="submit" class="btn btn-lg btn-default">
                                <i class="fa fa-redo"></i>
                            </button>
                        </div>
                    </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
    <!-- /.modal-dialog -->
    </div>
</div>
