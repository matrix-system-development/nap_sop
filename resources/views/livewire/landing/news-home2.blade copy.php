
<div>
    @foreach ($news2 as $nw2)
    <div class="col-6 col-md-12 mb-1">
        <div class="card border-0 rounded-0 text-white overflow zoom">
            <div class="position-relative">
                <div class="ratio_right-cover-2 image-wrapper">
                    <a href="{{route('news.id', ['id' => $nw2->id_post])}}">
                        <img class="img-fluid"
                            src="{{url('storage/'.$nw2->thumbnail_path)}}"
                            alt="simple blog template bootstrap"
                            style="width:100%;height:100%;object-fit: cover;">
                    </a>
                </div>
                <div class="position-absolute p-2 p-md-3 b-0 w-100 bg-shadow">
                    <a class="p-1 badge badge-primary rounded-0" href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">Lifestyle</a>
                    <a href="{{route('news.id', ['id' => $nw2->id_post])}}">
                        <h2 class="h5 text-white my-1">{{$nw2->title}}</h2>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>