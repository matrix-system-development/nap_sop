    
    <div>
        @php
            $count_carousel = 0;
        @endphp
        @foreach ($news as $nw)
            @if ($count_carousel == 0)
                <div class="carousel-item active">
            @else
                <div class="carousel-item">
            @endif
                <div class="card border-0 rounded-0 text-light overflow zoom">
                    <div class="position-relative">
                        <div class="ratio_left-cover-1 image-wrapper">
                            <a href="{{route('news.id', ['id' => $nw->id_post])}}">
                                <img class="img-fluid w-100"
                                    src="{{url('storage/'.$nw->thumbnail_path)}}"
                                    alt="Bootstrap news template"
                                    style="width: 30vw;
                                    height: 42vw;
                                    object-fit: cover;"
                                    >
                            </a>
                        </div>
                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                            <a href="{{route('news.id', ['id' => $nw->id_post])}}">
                                <h2 class="h3 post-title text-white my-1">{{$nw->title}}</h2>
                            </a>
                            <div class="news-meta">
                                {{-- <span class="news-author">by <a class="text-white font-weight-bold" href="../category/author.html">{{$nw->name_user}}</a></span> --}}
                                <span class="news-date">{{$nw->created_at}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @php
                $count_carousel++;
            @endphp
        @endforeach
</div>
