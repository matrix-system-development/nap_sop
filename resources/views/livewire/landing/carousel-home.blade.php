
<div>
    @foreach ($sops as $sp)
        <div class="item" style="background-color: rgb(45, 45, 190);">
            <div class="text">
                <a href="{{route('sop.title', ['title'=>$sp->seo_url])}}" style="color:white;">{{$sp->title}}</a>
            </div>
            <div class="overlay">
                <div class="text">
                    <a href="{{route('sop.title', ['title'=>$sp->seo_url])}}" style="color:white;">{{$sp->title}}</a>
                </div>
            </div>
        </div>
    @endforeach
</div>
