
<div>
    @foreach ($news2 as $nw2)
        <div class="single-blog video-style small row m_b_30">
            <div class="thumb col-12 col-sm-5">
                <a href="{{route('news.title', ['title' => $nw2->seo_url])}}">
                    <img class="img-fluid" src="{{url($nw2->thumbnail_path)}}" alt="">
                </a>
                
            </div>
            <div class="short_details col-12 col-sm-7">
                <a class="d-block" href="{{route('news.title', ['title' => $nw2->seo_url])}}">
                    <h4>{{$nw2->title}}</h4>
                </a>
                <div class="meta-bottom d-flex">
                    <a href="#"><i class="ti-time"></i>{{$nw2->created_at->isoFormat('dddd, D MMMM Y')}}</a>
                </div>
            </div>
        </div> 
    @endforeach
</div>