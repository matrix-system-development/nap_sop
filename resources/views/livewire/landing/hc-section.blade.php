    
<div>
    @php
        $count_carousel_hc = 0;
    @endphp
    <div id="hc-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
    @foreach ($corners as $cr)
    @if ($count_carousel_hc == 0)
        <div class="carousel-item active">
    @else
        <div class="carousel-item">
    @endif
        <div class="position-relative">
            <div class="single-blog video-style">
                <div class="thumb">
                    <a href="{{route('hc-corner.title', ['title'=>$cr->seo_url])}}">
                        <img class="img-fluid" src="{{url($cr->thumbnail_path)}}" alt="" >
                    </a>
                    
                </div>
                <div class="short_details">
                    <div class="meta-top d-flex">
                        <a href="#">{{$cr->created_at->format('l, d M Y')}}</a>
                    </div>
                    <a class="d-block" href="{{route('hc-corner.title', ['title'=>$cr->seo_url])}}">
                        <h4>{{$cr->title}}</h4>
                    </a>
                </div>
            </div> 
        </div>
    </div>
    @php
        $count_carousel_hc++;
    @endphp
    @endforeach
        </div>
        <a class="carousel-control-prev" href="#hc-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#hc-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
