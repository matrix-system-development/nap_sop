    
<div>
    @php
        $count_carousel = 0;
    @endphp
    <div id="news-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
    @foreach ($news as $nw)
    @if ($count_carousel == 0)
        <div class="carousel-item active">
    @else
        <div class="carousel-item">
    @endif
        <div class="position-relative">
            <div class="single-blog video-style">
                <div class="thumb">
                    <a href="{{route('news.title', ['title' => $nw->seo_url])}}">
                        <img class="img-fluid" src="{{url($nw->thumbnail_path)}}" alt="" >
                    </a>
                </div>
                <div class="short_details">
                    <div class="meta-top d-flex">
                        <a href="#">{{$nw->created_at->isoFormat('dddd, D MMMM Y')}} </a>
                    </div>
                    <a class="d-block" href="{{route('news.title', ['title' => $nw->seo_url])}}">
                        <h4>{{$nw->title}}</h4>
                    </a>
                </div>
            </div> 
        </div>
    </div>
    @php
        $count_carousel++;
    @endphp
    @endforeach
        </div>
        <a class="carousel-control-prev" href="#news-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: blue"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#news-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: blue"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
