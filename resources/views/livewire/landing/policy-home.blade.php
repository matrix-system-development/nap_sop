
<div>
    @foreach ($policy as $pol)
        <div class="item" style="background-color: rgb(45, 45, 190);">
            <div class="text">
                <a href="{{route('policy.title', ['title'=>$pol->seo_url])}}" style="color:white;">{{$pol->title}}</a>
            </div>
            <div class="overlay">
                <div class="text">
                    <a href="{{route('policy.title', ['title'=>$pol->seo_url])}}" style="color:white;">{{$pol->title}}</a>
                </div>
            </div>
        </div>
    @endforeach
</div>
