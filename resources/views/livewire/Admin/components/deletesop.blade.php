<div>
    <div class="row">
        <div class="col-sm-6 col-md-6 py-4 px-3">
          <h4 class="h4-responsive">SOP</h4>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="md-form">
            <input wire:model="searchSOP" placeholder="Search..." type="text" id="form5" class="form-control">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 pb-3">

          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    SOP Name
                  </th>
                  <th>Department</th>
                  <th>Upload Date</th>
                  <th>URL</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($sop as $sp)
                  <tr>
                    <th scope="row">
                      <label class="form-check-label" for="checkbox{{$sp->id}}">{{$sp->title}}</label>
                      {{-- <fieldset class="form-check">
                        <input class="form-check-input" type="checkbox" id="checkbox{{$sp->id}}" wire:model="selected" value="{{$sp->id}}" checked="checked">

                      </fieldset> --}}
                    </th>
                    <td>{{$sp->dept_name}}</td>
                    <td><span class="grey-text"><small><i class="far fa-clock-o"></i> {{$sp->created_at}}</small></span></td>
                    <td><a href="{{$sp->file_path}}">Click</a></td>

                    <td>
                        {{-- <a class="btn btn-danger" wire:click="$emitTo('components.sop.modal-delete-sop', 'details-delete-show', {{$sp->id}})">Delete</a> --}}
                        <a href="" class="btn btn-danger btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#centralModalDangerDemo" wire:click.prevent="$emitTo('components.sop.modal-delete-sop', 'details-delete-show', {{$sp->id}})">Delete</a>
                        {{-- <a href="" class="btn btn-danger btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#centralModalDangerDemo" wire:click.prevent="selectDelete({{$sp->id}})">Delete</a> --}}
                        {{-- <a href="" class="btn btn-danger btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#centralModalDangerDemo" wire:click.prevent="selectDelete({{$sp->id}})" :key="$sp->id">Delete</a> --}}
                    </td>
                  </tr>
                  @endforeach

              </tbody>
            </table>
          </div>
          {{ $sop->links() }}
        </div>
      </div>
      <div class="modal fade" id="centralModalDangerDemo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-notify modal-danger" role="document">
          <!-- Content -->
          <div class="modal-content">
            <!-- Header -->
            <div class="modal-header">
              <p class="heading">Confirm Delete</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="white-text">×</span>
              </button>
            </div>
            <!-- Body -->
            <div class="modal-body">
              <div class="row">
                <div class="col-3">
                  <p class="text-center"><i class="fas fa-trash fa-4x"></i></p>
                </div>

                <div class="col-9">
                    <p>Are You sure to delete this file?</p>
                    <div>
                        @livewire('components.sop.modal-delete-sop')
                    </div>

                    {{-- <p>{{$selected_delete}}</p> --}}
                    {{-- <span>{{$selected_delete}}</span> --}}
                </div>
              </div>
            </div>

            <!-- Footer -->
            <div class="modal-footer justify-content-center">
              <a type="button" class="btn btn-danger waves-effect waves-light" wire:click="$emitTo('components.sop.modal-delete-sop', 'confirm-delete')" >Delete</i></a>
              <a type="button" class="btn btn-outline-danger waves-effect" data-dismiss="modal">Cancel</a>
            </div>
          </div>
          <!-- Content -->
        </div>
      </div>

</div>
