<div>
    <div class="form-group">
        <label for="chooseFile">File</label>
        <div class="input-group">
          <div class="custom-file">
            <label class="custom-file-label" for="chooseFile" x-data="{ files: null }">
            <input type="file" class="sr-only" name="file" id="chooseFile" wire:model="pdfPreview" x-on:change="files = Object.values($event.target.files)">
            <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose file...'"></span></label>
          </div>
        </div>
    </div>
    @if ($pdfPreview)
    <div class="row">
        <div class="col-12">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Preview</h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                          </button>
                          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Cloe">
                            <i class="fas fa-times"></i>
                          </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <iframe src="{{ $pdfPreview->temporaryUrl() }}" style="width: 100%;height: 800px;border: none;"></iframe>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    @endif
</div>


