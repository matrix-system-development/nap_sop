<div>
    <form wire:submit.prevent="update" enctype="multipart/form-data">
        <input type="hidden" wire:model="selected_id">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" wire:model.lazy="title" placeholder="Enter Title" required readonly>
        </div>
        <div class="form-group">
            <div class="row">
                    <div class="col-8">
                        <label>Remarks</label>
                        <textarea class="form-control" wire:model.lazy="desc" name="desc" rows="3" placeholder="Enter ..."></textarea>
                    </div>
                    <div class="col-4">
                        <label for="dept">Department</label>
                        <select class="form-control" name="dept" wire:model.lazy="dept" id="" required readonly>
                            <option value="{{$dept}}" selected>{{$dept}}</option>
                        </select>
                    </div>
            </div>
       </div>
        <div class="form-group">
            <label for="chooseFile">File</label>
            <div class="input-group">
              <div class="custom-file">
                <label class="custom-file-label" for="chooseFile" x-data="{ files: null }">
                <input type="file" class="sr-only" name="file" id="chooseFile" wire:model.lazy="file" x-on:change="files = Object.values($event.target.files)">
                <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose file...'"></span></label>
              </div>
            </div>
        </div>
        @if ($file)
        {{-- <div class="row">
            <div class="col-12">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Preview</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Cloe">
                                <i class="fas fa-times"></i>
                              </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <iframe src="{{ $file->temporaryUrl() }}" style="width: 100%;height: 800px;border: none;"></iframe>
                        </div>
                    </div> 
                </div>
            </div>
        </div> --}}
            {{-- @livewire('preview-pdf',['file' => $file]) --}}
            <livewire:components.sop.preview-pdf :previewFile="$file" :wire:key="$file->temporaryUrl()">
        @endif
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
