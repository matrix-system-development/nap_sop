<div>
    <section class="mb-5">
        {{-- <h3 id="" class="font-weight-bold dark-grey-text mb-4"><strong>Image overlay</strong></h3> --}}
        <div class="row">
            @foreach ($query as $sp)
            <div class="col-md-6 my-3">
                <div class="card shadow border-0 rounded-0 text-light overflow zoom">
                    <div class="position-relative">
                        <div class="ratio_right-cover-2 image-wrapper">
                            <a href="{{route('sop.title', ['title'=>$sp->seo_url])}}" target="_blank" rel="noopener noreferrer">
                                <div class="pdf-thumbnail">
                                    <img class="img-fluid"
                                        data-pdf-thumbnail-file="{{url('/view/isolatethumbnail/'.$sp->filename)}}"
                                        alt="simple blog template bootstrap"
                                        style="width:100%;height:100%;object-fit: cover;">
                                </div>
                            </a>
                        </div>
                        <div class="position-absolute p-2 p-md-3 b-0 w-100 bg-shadow">
                            <a href="">
                                <h2 class="h5 text-white my-1">{{$sp->title}}</h2>
                            </a>
                        </div>
                    </div>
                </div> 
            </div>
            @endforeach
        </div>
    </section>
    
    {{$query->links() }}
</div>
