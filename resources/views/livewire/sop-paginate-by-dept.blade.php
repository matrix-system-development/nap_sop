
  <div>
    <section class="mt-md-4 pt-md-2 mb-3 my-3">
      <div class="container-fluid">
          <div class="card card-cascade narrower">
            <section>
              <div class="row">
                <div class="col-12 mr-0">
                  <div class="row">
                    <div class="col-12">
                      <section class="row">
                        <div class="col-12 col-md-12">
                          <div class="view view-cascade gradient-card-header blue-gradient">
                            <div class="row">
                              <div class="col-md-3">
                                <label for="">Sort</label>
                                <select class="form-control" wire:model="sortParam">
                                  <option value="" hidden>..</option>
                                  <option value="title">Title</option>
                                  <option value="created_at">Created Date</option>
                                </select>
                    
                              </div>
                              <div class="col-md-3">
                                <label for="">Direction</label>
                                <select class="form-control" wire:model="sortDir">
                                  <option value="" hidden>..</option>
                                  <option value="ASC">Ascending</option>
                                  <option value="DESC">Descending</option>
                                </select>
                              </div>
                              <div class="col-md-6">
                                <label for="">Search</label>
                                <input type="text" class="form-control" placeholder="Search Title.." wire:model.debounce.1000ms="searchTerm" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                  <div class="card-body card-body-cascade pb-0">
                    <div class="container-fluid">
                      <div class="row card-body pt-1">
                        <div class="col-12">
                          <div class="row">
                            @foreach ($query as $sp)
                            <div class="col-12 col-md-4 my-3">
                                <section>
                                  <div class="card card-cascade narrower" style="margin-top: 44px">
                                    <div class="view view-cascade overlay">
                                        <div class="mask rgba-white-slight"></div>
                                      <a>
                                        <div class="mask rgba-white-slight"></div>
                                      </a>
                                    </div>
                                    <div class="card-body card-body-cascade">
                                      <h5 class="pink-text"> {{$sp->dept_name}}</h5>
                                      <h3 class="card-title"><a href="{{route('sop.title', ['title'=>$sp->seo_url])}}">{{$sp->title}}</a></h3>
                                      <h6>{{$sp->created_at->isoFormat('dddd, D MMMM Y')}}</h6>
                                      <a class="btn btn-unique waves-effect waves-light" href="{{route('sop.title', ['title'=>$sp->seo_url])}}" target="_blank" rel="noopener noreferrer">Read</a>
                                    </div>
                                  </div>
                                </section>
                            </div>
                            @endforeach
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
      </div>
    </section>
    {{$query->links() }}
  </div>
