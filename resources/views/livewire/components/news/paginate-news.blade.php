<div>
    
    <section class="row">
        <div class="col-md-2">
            <select class="form-control" wire:model="sortParam">
                <option value="" hidden>Sort By</option>
                <option value="created_at">Created Date</option>
                <option value="title">Title</option>
            </select>

        </div>
        <div class="col-md-2">
            <select class="form-control" wire:model="sortDir">
                <option value="" hidden>Direction</option>
                <option value="ASC">Ascending</option>
                <option value="DESC">Descending</option>
            </select>
        </div>
    </section>

    @foreach ($news as $nw)
    <div class="row">
        <div class="col-md-6">
            <h2>
                <a href="{{route('news.title', $nw->seo_url)}}" target="_blank" rel="noopener noreferrer">
                    {{$nw->title}}
                </a>
            </h2>
            <br>
            <span>{{$nw->created_at->isoFormat('dddd, D MMMM Y')}}</span>
            <br>
            <a href="{{route('news.title', $nw->seo_url)}}" class="btn btn-primary" target="_blank" rel="noopener noreferrer">Read More</a>
        </div>
        <div class="col-md-6">
            <a href="{{route('news.title', $nw->seo_url)}}"  target="_blank" rel="noopener noreferrer">
                <img src="{{url($nw->thumbnail_path)}}" alt=""/>
            </a>
            
        </div>
    </div> 
    @endforeach
    <div class="row">
        {{$news->links() }} 
    </div> 
    
</div>
