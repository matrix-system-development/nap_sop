<div>
<div class="container-fluid">
        <h2 class="text-center display-4">Search</h2>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                    <div class="input-group">
                        <input wire:model="search" type="text" class="form-control form-control-lg" placeholder="Type your keywords here" name="search">
                    </div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-9">
        @if($posts->count())
        <table class="table">
            <thead>
                <tr>
                    <th>
                        <a wire:click.prevent="sortBy('title')" role="button" href="#">
                            Title
                            @include('Layouts.Includes.sort-icon', ['field' => 'title'])
                        </a>
                    </th>
                    <th>
                        <a wire:click.prevent="sortBy('dept_name')" role="button" href="#">
                            Department Name
                            @include('Layouts.Includes.sort-icon', ['field' => 'dept_name'])
                        </a>
                    </th>
                    <th>
                        <a wire:click.prevent="sortBy('status')" role="button" href="#">
                            status
                            @include('Layouts.Includes.sort-icon', ['field' => 'status'])
                        </a>
                    </th>
                    <th>
                        <a wire:click.prevent="sortBy('created_at')" role="button" href="#">
                        Created at
                        @include('Layouts.Includes.sort-icon', ['field' => 'created_at'])
                        </a>
                    </th>
                    <th>
                        Delete
                    </th>
                    <th>
                        Edit
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->dept_name }}</td>
                        <td>{{ $post->status }}</td>
                        <td>{{ $post->created_at->format('m-d-Y') }}</td>
                        <td>
                            <button class="btn btn-sm btn-danger">
                            Delete
                            </button>
                        </td>
                        <td>
                            <button class="btn btn-sm btn-dark">
                            Edit
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <div class="alert alert-warning">
                Your query returned zero results.
            </div>
        @endif
</div>
    </div>

    <div class="row">
        <div class="col">

        </div>
    </div>
</div>
</div>

