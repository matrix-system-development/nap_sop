<div>
    <div class="row">
        <fieldset class="form-check mt-2">
            <input class="form-check-input" type="checkbox" id="checkbox1" wire:model.defer="selectedContent"  value="sop">
            <label class="form-check-label" for="checkbox1">SOP</label>
        </fieldset>
        <fieldset class="form-check mt-2">
            <input class="form-check-input" type="checkbox" id="checkbox2" wire:model.defer="selectedContent" value="policy">
            <label class="form-check-label" for="checkbox2">Company Policy</label>
        </fieldset>
        {{-- <div>@json($selectedContent)</div> --}}
    </div>
    @if (!empty($contents))
        @php
            $title = ''
        @endphp
        @php
            $counter = 0
        @endphp
        @foreach ($contents as $ctn)
        @if(($ctn->title != $title) and ($counter == 0))
            @php
                $title = $ctn->title
            @endphp
            @php
                $counter++
            @endphp
        <div class="row">
            <div class="card-body">
                <div class="flex-fill">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="card-title">
                                        @if (isset($ctn->dept_name))
                                        <a href="{{route('sop.title', ['title' =>$ctn->seo_url])}}" style="color:white;">{{ $ctn->title }} - {{'SOP '.$ctn->dept_name}}</a>
                                        @else
                                        <a href="{{route('policy.title', ['title' =>$ctn->seo_url])}}" style="color:white;">{{ $ctn->title }} - Company Policy</a>
                                        @endif
                                    </h2>
                                </div>
                                <div class="card-body">
                                    @if (strlen($ctn->content) > 1)
                                    <div class="row">
                                        <div id="accordion{{ $ctn->page }}" class="col-12">
                                            <div class="card card-primary card-outline">
                                                <a class="d-block w-100" >
                                                    <div class="card-header">
                                                        <h4 class="card-title w-100">
                                                            @if (isset($ctn->dept_name))
                                                            <a href="{{route('sop.id.page', ['title'=>$ctn->seo_url , 'page'=>$ctn->page])}}" style="color:white;"> {{ $ctn->title }} - Page {{ $ctn->page }}</a>
                                                            @else
                                                            <a href="{{route('policy.id.page', ['title'=>$ctn->seo_url , 'page'=>$ctn->page])}}" style="color:white;"> {{ $ctn->title }} - Page {{ $ctn->page }}</a>
                                                            @endif
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div data-parent="#accordion{{ $ctn->page }}" style="">
                                                    <div class="card-body">
                                                        <livewire:components.landing.highlight-search :text="$ctn->content" :title="$ctn->seo_url" :pageNum="$ctn->page" :searched="$search" :dept="$ctn->dept_name" :wire:key="$ctn->page">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

        @elseif($title == $ctn->title)
            @if (strlen($ctn->content) > 1)
            <div class="row">
                <div id="accordion{{ $ctn->page }}" class="col-12">
                    <div class="card card-primary card-outline">
                        <a class="d-block w-100" >
                            <div class="card-header">
                                <h4 class="card-title w-100">
                                    @if (isset($ctn->dept_name))
                                    <a href="{{route('sop.id.page', ['title'=>$ctn->seo_url , 'page'=>$ctn->page])}}" style="color:white;"> {{ $ctn->title }} - Page {{ $ctn->page }}</a>
                                    @else
                                    {{$ctn->dept_name}}
                                    <a href="{{route('policy.id.page', ['title'=>$ctn->seo_url , 'page'=>$ctn->page])}}" style="color:white;"> {{ $ctn->title }} - Page {{ $ctn->page }}</a>
                                    {{-- <a href="{{route('policy.id.page', ['title' =>$ctn->seo_url])}}" style="color:white;">{{ $ctn->title }} - Company Policy</a> --}}
                                    @endif
                                </h4>
                            </div>
                        </a>
                        <div data-parent="#accordion{{ $ctn->page }}" style="">
                            <div class="card-body" id="content-fetch">
                                <livewire:components.landing.highlight-search :text="$ctn->content" :title="$ctn->seo_url" :pageNum="$ctn->page" :searched="$search" :dept="$ctn->dept_name" :wire:key="$ctn->page">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @php
        $counter++
        @endphp
        @elseif(($ctn->title != $title) and ($counter != 0))
        @php
        $title = $ctn->title
        @endphp
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card-body">
                <div class="flex-fill">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="card-title">
                                        @if (isset($ctn->dept_name))
                                        <a href="{{route('sop.title', ['title' =>$ctn->seo_url])}}" style="color:white;">{{ $ctn->title }} - {{'SOP '.$ctn->dept_name}}</a>
                                        @else
                                        <a href="{{route('policy.title', ['title' =>$ctn->seo_url])}}" style="color:white;">{{ $ctn->title }} - Company Policy</a>
                                        @endif
                                    </h2>
                                </div>
                                <div class="card-body">
                                    @if (strlen($ctn->content) > 1)
                                    <div class="row">
                                        <div id="accordion{{ $ctn->page }}" class="col-12">
                                            <div class="card card-primary card-outline">
                                                <a class="d-block w-100" >
                                                    <div class="card-header">
                                                        <h4 class="card-title w-100">
                                                            <a href="{{route('sop.id.page', ['title'=>$ctn->seo_url , 'page'=>$ctn->page])}}" style="color:white;"> {{ $ctn->title }} - Page {{ $ctn->page }}</a>
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div data-parent="#accordion{{ $ctn->page }}" style="">
                                                    <div class="card-body">
                                                        <span>

                                                        </span>
                                                        <livewire:components.landing.highlight-search :text="$ctn->content" :title="$ctn->seo_url" :pageNum="$ctn->page" :searched="$search" :dept="$ctn->dept_name" :wire:key="$ctn->page">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
        @endif
        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
