<div>
    
    <section class="row">
        <div class="col-md-2">
            <select class="form-control" wire:model="sortParam">
                <option value="" hidden>Choose One</option>
                <option value="created_at">Created Date</option>
                <option value="title">Title</option>
            </select>

        </div>
        <div class="col-md-2">
            <select class="form-control" wire:model="sortDir">
                <option value="" hidden>Choose One</option>
                <option value="ASC">Ascending</option>
                <option value="DESC">Descending</option>
            </select>
        </div>
    </section>

    @foreach ($tips as $tp)
    <div class="row">
        <div class="col-md-6">
            <h2>
                <a href="{{route('tips.title',$tp->seo_url)}}" target="_blank" rel="noopener noreferrer">
                    {{$tp->title}}
                </a>
            </h2>
            <span>{{$tp->created_at->isoFormat('dddd, D MMMM Y')}}</span>
            <br>
            <a href="{{route('tips.title',$tp->seo_url)}}" class="btn btn-primary"  target="_blank" rel="noopener noreferrer">Read More</a>
        </div>
        <div class="col-md-6"  target="_blank" rel="noopener noreferrer">
            <a href="{{route('tips.title',$tp->seo_url)}}">
                <img src="{{url($tp->thumbnail_path)}}" alt=""/>
            </a>
            
        </div>
    </div> 
    @endforeach
    <div class="row">
        {{$tips->links() }} 
    </div> 
    
</div>
