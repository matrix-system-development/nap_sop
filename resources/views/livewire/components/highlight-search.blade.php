<div>
    <p>
        @if ($dept)
        <a href="{{route('sop.id.page', ['title'=>$title , 'page'=>$pageNum])}}" style="color: black;">{!! $contents !!}</a>
        @else
        <a href="{{route('policy.id.page', ['title'=>$title , 'page'=>$pageNum])}}" style="color: black;">{!! $contents !!}</a>
        @endif
    </p>
</div>
