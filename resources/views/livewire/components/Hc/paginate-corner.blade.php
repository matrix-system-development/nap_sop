<div>
    
    <section class="row">
        <div class="col-md-2">
            <select class="form-control" wire:model="sortParam">
                <option value="" hidden>Choose One</option>
                <option value="created_at">Created Date</option>
                <option value="title">Title</option>
            </select>

        </div>
        <div class="col-md-2">
            <select class="form-control" wire:model="sortDir">
                <option value="" hidden>Choose One</option>
                <option value="ASC">Ascending</option>
                <option value="DESC">Descending</option>
            </select>
        </div>
    </section>

    @foreach ($hcorners as $hc)
    <div class="row">
        <div class="col-md-6">
            <h2>
                <a href="{{route('hc-corner.title', $hc->seo_url)}}" target="_blank" rel="noopener noreferrer">
                    {{$hc->title}}
                </a>
            </h2>
            <br>
            <span>{{$hc->created_at->isoFormat('dddd, D MMMM Y')}}</span>
            <br>
            <a href="{{route('hc-corner.title', $hc->seo_url)}}" class="btn btn-primary" target="_blank" rel="noopener noreferrer">Read More</a>
            
        </div>
        <div class="col-md-6">
            <a href="{{route('hc-corner.title', $hc->seo_url)}}" target="_blank" rel="noopener noreferrer">
                <img src="{{url($hc->thumbnail_path)}}" alt=""/>
            </a>
            
        </div>
    </div> 
    @endforeach
    <div class="row">
        {{$hcorners->links() }} 
    </div> 
    
</div>
