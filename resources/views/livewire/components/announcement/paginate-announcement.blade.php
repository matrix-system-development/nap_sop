<div>
    {{-- <div class="row">
        <div class="col-">
                <div class="card-body white-text">
                    <div class="row">
                        <div class="col-md-6 col-6 indigo">
                            <label for="">Sort</label>
                            <select class="form-control" wire:model="sortParam">
                                <option value="" hidden>Choose One</option>
                                <option value="created_at">Created Date</option>
                                <option value="title">Title</option>
                            </select>

                        </div>
                        <div class="col-md-6 col-6 indigo">
                            <label for="">Direction</label>
                            <select class="form-control" wire:model="sortDir">
                                <option value="" hidden>Choose One</option>
                                <option value="ASC">Ascending</option>
                                <option value="DESC">Descending</option>
                            </select>
                        </div>
                    </div>
                </div>
        </div>
    </div> --}}
    @foreach ($announcement as $ann)
    <div class="row">
        <div class="col-md-6">
            <h2>
                <a href="{{route('announcement.title',$ann->seo_url)}}" target="_blank" rel="noopener noreferrer">
                    {{$ann->title}}
                </a>
            </h2>
            <br>
            <span>{{$ann->created_at->isoFormat('dddd, D MMMM Y')}}</span>
            <br>
            <a href="{{route('announcement.title',$ann->seo_url)}}" class="btn btn-primary" target="_blank" rel="noopener noreferrer">Read More</a>
        </div>
        <div class="col-md-6">
            <a href="{{route('announcement.title',$ann->seo_url)}}" target="_blank" rel="noopener noreferrer">
                <img src="{{url($ann->thumbnail_path)}}" alt=""/>
            </a>

        </div>
    </div>
    @endforeach
    <div class="row">
        {{$announcement->links() }}
    </div>

</div>
