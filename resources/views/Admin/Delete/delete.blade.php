@extends('Layouts.master')

@section('content')
<div class="content-wrapper">
    @if (session()->has('error-delete'))
    <script>
        toastr.success({{ session('error-delete') }});
    </script>
    @endif
    @if (session()->has('success-delete'))
    <script>
        toastr.success({{ session('success-delete') }});
    </script>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Delete SOP</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        @if (\Session::has('success'))
          <div class="alert alert-success">
              <ul>
                  <li>{!! \Session::get('msg') !!}</li>
              </ul>
          </div>
        @endif
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card mx-auto white z-depth-1">
                <div class="card-body">
                    <div class="col-md-12 mx-auto white z-depth-1">
                        @livewire('components.sop.delete-sop')
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
{{-- <script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script> --}}
{{-- <script src="{{ asset('plugins/resumablejs/resumable.min.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('diy/js/upload-split.js') }}"></script> --}}
@endsection

