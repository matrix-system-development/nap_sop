@extends('Layouts.master')

@section('content')
<div class="content-wrapper pl-3 pr-3">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Company Policy</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        @if (\Session::has('success'))
          <div class="alert alert-success">
              <ul>
                  <li>{!! \Session::get('msg') !!}</li>
              </ul>
          </div>
        @endif
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                    {{-- <livewire:components.sop.postsop /> --}}
                    <form action="{{route('admin.policy.uploadPolicy')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <div class="row">
                          <div class="col-lg-6 col-12">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" required>
                            @error('title') <span class="error">{{ $message }}</span> @enderror
                          </div>
                          <div class="col-lg-6 col-12">
                            <label for="browseFile">File</label>
                            <div class="input-group">
                              <div class="custom-file">
                                <label class="custom-file-label" for="browseFile" x-data="{ files: null }">
                                <input type="file" class="sr-only" name="file" id="browseFile" x-on:change="files = Object.values($event.target.files)">
                                <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose file...'"></span></label>
                                <a id="url_upload" upload-route="{{route('admin.policy.uploadSplitPolicy') }}" csrf_token="{{csrf_token()}}" hidden></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="container-fluid pt-4 pl-0">
                      <div class="row justify-content-left">
                          <div class="col-md-12">
                              <div class="card">
                                  <div class="card-header text-center">
                                      <h5>Preview</h5>
                                  </div>
                                  <div class="card-body">
                                      <div  style="display: none" class="progress mt-3" style="height: 25px">
                                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%; height: 100%">75%</div>
                                      </div>
                                  </div>
                                  <div class="card-footer p-4" style="display: none">
                                    <iframe id="pdfPreview" src=""  style="width: 100%;height: 800px;border: none;"></iframe>
                                    <input name="urlPdf" id="urlPdf" type="text" value="" hidden>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  <div class="row pt-4 pl-0">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script>
<script src="{{ asset('plugins/resumablejs/resumable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('diy/js/upload-split-policy.js') }}"></script>
@endsection

