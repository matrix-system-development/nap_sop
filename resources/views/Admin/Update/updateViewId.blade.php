@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Update SOP - {{$posts->title}}</h1>
          </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{$posts->title}}</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse" >
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <livewire:components.sop.updatesop :selected_id="$posts->id_post" :wire:key="$posts->id_post">
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
    <!-- /.content -->
</div>
@livewireScripts

<script type="text/javascript">

  function fetchConvert() {
    var x = document.getElementById("convertedname");
    var y = document.getElementById("customCheckbox1");
    window.livewire.emit('get-converted', { filename : x.value, check : y.value});
  };
</script>
@endsection