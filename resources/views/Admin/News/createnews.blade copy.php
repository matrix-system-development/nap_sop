@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Create News</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content create">
      <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Form</h3>  
                </div>
                <div class="card-body">
                  
              <textarea id="summernote">
               
              </textarea>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
<script>
  $(function () {
    $('#summernote').summernote({
      tabsize: 3,
      height: 600,
      focus: true,
      toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
      ]
    })
  })
</script>

@endsection

