@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Create Content</h1>     
          </div>
        </div>
      </div>
    </div>
    <section class="content create">
      <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <livewire:components.hc.hc-corner-create />
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote-cleaner.js') }}"></script>
<script src="{{ asset('diy/js/summernote-config.js') }}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection

