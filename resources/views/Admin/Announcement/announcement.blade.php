@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Create Announcement</h1>     
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content create">
      <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <livewire:components.announcement.create-announcement />
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote-cleaner.js') }}"></script>
<script src="{{ asset('diy/js/summernote-config.js') }}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection

