@extends('Layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">SOP File</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        @if (\Session::has('success'))
          <div class="alert alert-success">
              <ul>
                  <li>{!! \Session::get('msg') !!}</li>
              </ul>
          </div>
        @endif
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.sop.uploadSOP')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" required>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-12">
                                      <label for="browseFile">File</label>
                                      <div class="input-group">
                                        <div class="custom-file">
                                          <label class="custom-file-label" for="browseFile" x-data="{ files: null }">
                                          <input type="file" class="sr-only" name="file" id="browseFile" x-on:change="files = Object.values($event.target.files)">
                                          <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose file...'"></span></label>
                                          <a id="url_upload" upload-route="{{route('admin.sop.uploadSplitSOP') }}" csrf_token="{{csrf_token()}}" hidden></a>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="dept">Department</label>
                                        <select class="form-control" name="dept" id="dept_owner" required>
                                                <option value="" hidden>Select a Department</option>
                                                @foreach($dept as $d)
                                                  <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                                                @endforeach
                                        </select>
                                        @error('dept') <span class="text-danger">Please select a department!</span> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5" x-data="{private: false}">
                            <div class="col-12">
                                <input class="form-check-input" type="checkbox" value="checked" id="flexCheckDefault" name="privateSop" x-model="private">
                                <label class="form-check-label" for="flexCheckDefault"  name="privateSop">
                                    Private / View Access for Selected Department Only
                                </label>
                            </div>
                            <div class="col-12" x-show="private">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="dept">Select Department</label>
                                                <select class="form-control" name="dept_private" id="dept">
                                                    <option value="" hidden>Select a Department</option>
                                                    @foreach($dept as $d)
                                                    <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                                                    @endforeach
                                                </select>
                                                <a class="btn btn-primary" id="btnAdd">Add View Access</a>
                                            </div>
                                            <div class="col-6">
                                                <label for="list">Department List:</label>
                                                <select class="form-control" id="list" name="list" multiple></select>
                                                <a class="btn btn-danger" id="btnRemove">Remove Selected Department</a>
                                                <input type="text" name="dept_selected" id="dept_selected" hidden>
                                            </div>
                                        </div>
                                        @error('dept') <span class="text-danger">Please select a department!</span> @enderror
                                    </div>
                                </div>
                                <script>
                                    const btnAdd = document.querySelector('#btnAdd');
                                    const btnRemove = document.querySelector('#btnRemove');
                                    const sb = document.querySelector('#list');
                                    const name = document.querySelector('#dept');
                                    const owner = document.querySelector('#dept_owner');
                                    var isexist;
                                    var list_selected = [];
                                    btnAdd.onclick = (e) => {
                                        e.preventDefault();
                                        isexist = false;
                                        if (name.value == '') {
                                            alert('Please enter the name.');
                                            return;
                                        }
                                        if(name.value == owner.value) {
                                            alert('You have already specified this department!');
                                            return;
                                        }
                                        for (i = 0; i < document.getElementById("list").length; ++i){
                                            if (document.getElementById("list").options[i].value == name.value){
                                                alert("You cannot input department twice!");
                                                isexist = true;
                                                break;
                                            }
                                        }
                                        if (isexist == false) {
                                            const option = new Option(name.value, name.value);
                                            sb.add(option, undefined);
                                            // document.getElementById("dept_selected").value += name.value + ',';
                                            list_selected.push(name.value);
                                            document.getElementById("dept_selected").value = list_selected;
                                            // console.log(list_selected);
                                            name.value = '';
                                            name.focus();
                                        }

                                    };

                                    btnRemove.onclick = (e) => {
                                        e.preventDefault();
                                        let selected = [];
                                        for (let i = 0; i < sb.options.length; i++) {
                                            selected[i] = sb.options[i].selected;
                                        }
                                        let index = sb.options.length;
                                        while (index--) {
                                            if (selected[index]) {
                                                sb.remove(index);
                                                list_selected.splice(index, 1);
                                                document.getElementById("dept_selected").value = list_selected;
                                                // console.log(list_selected);
                                            }
                                        }
                                    };
                                </script>
                            </div>
                        </div>
                      </div>
                    <div class="container-fluid pt-4 pl-0">
                      <div class="row justify-content-left">
                          <div class="col-md-12">
                              <div class="card">
                                  <div class="card-header text-center">
                                      <h5>Preview</h5>
                                  </div>
                                  <div class="card-body">
                                      <div  style="display: none" class="progress mt-3" style="height: 25px">
                                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%; height: 100%">75%</div>
                                      </div>
                                  </div>
                                  <div class="card-footer p-4" style="display: none">
                                    <iframe id="pdfPreview" src=""  style="width: 100%;height: 800px;border: none;"></iframe>
                                    <input name="urlPdf" id="urlPdf" type="text" value="" hidden>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row pt-4 pl-0">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>

<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script>
<script src="{{ asset('plugins/resumablejs/resumable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('diy/js/upload-split.js') }}"></script>
@endsection

