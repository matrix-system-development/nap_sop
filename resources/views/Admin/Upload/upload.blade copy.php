@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Upload File SOP</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Upload</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- /.card-body -->
              <form action="{{route('uploadSOP')}}" method="post" enctype="multipart/form-data">
                <div class="card-body">
                @csrf
                @if ($message = Session::get('success'))
                  <div class="alert alert-success">
                      <strong>{{ $message }}</strong>
                  </div>
                @endif
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                @endif
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" required>
                  </div>
                  <div class="form-group">
                    <label for="depts">Department</label>
                    <select class="form-control" name="dept" id="">
                      <option disabled selected value="">Select a Department</option>
                      @foreach($dept as $d)
                        <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="privacy">Type</label>
                    <select class="form-control" name="privacy" id="">
                      <option disabled selected value="">Select a type</option>
                        <option value="public">Public</option>
                        <option value="private">Private</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="chooseFile">File</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <label class="custom-file-label" for="chooseFile" x-data="{ files: null }">
                        <input type="file" class="sr-only" name="file" id="chooseFile" x-on:change="files = Object.values($event.target.files)">
                        <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose file...'"></span></label>
                      </div>
                      <!-- <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div> -->
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

