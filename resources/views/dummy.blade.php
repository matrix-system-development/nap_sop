<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">

    <title></title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="card">
        <div class="card-body">
            @if($currentUserInfo)
                <h4>IP: {{ $currentUserInfo }}</h4>
            @endif
        </div>
    </div>
</div>
</body>

</html>