<header>
    <div id="slide-out" class="side-nav wide sn-bg-4">
      <ul class="custom-scrollbar">
        <li class="logo-sn waves-effect py-4">
          <div class="text-center">
            <a href="{{route('home')}}" class="pl-0"><img src="{{asset('logo/matrix-logo.png')}}"></a>
          </div>
        </li>
        <li>
          <form class="search-form" role="search">
            <div class="md-form mt-0 waves-light">
              <input type="text" class="form-control py-2" name="searchModal" id="searchModal"
              placeholder="Search SOP" data-toggle="modal" data-target="#searchResult"
              autocomplete="off" readonly="readonly">
            </div>
          </form>
        </li>
        <li>
          <ul class="collapsible collapsible-accordion">
            <li>
                <a href="{{route('home')}}" class="collapsible-header waves-effect"><i class="w-fa fas fa-home"></i>Home</a>
            </li>
            <li>
              <a href="{{route('sop.all')}}" class="collapsible-header waves-effect"><i class="w-fa fas fa-file-pdf"></i>SOP</a>
            </li>
            @if(Auth::user()->hasAnyRole(array('admin', 'superadmin')))
            <li>
                <a class="collapsible-header waves-effect arrow-r">
                  <i class="w-fa far fa-check-square"></i>Admin Section<i class="fas fa-angle-down rotate-icon"></i>
                </a>
                <div class="collapsible-body">
                  <ul>
                    @if(Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('add.user')}}" class="waves-effect">Add User</a>
                    </li>
                    <li>
                      <a href="{{route('update.user')}}" class="waves-effect">Update Password</a>
                    </li>
                    <li>
                      <a href="{{route('view.activity')}}" class="waves-effect">Activity Log</a>
                    </li>
                    <li>
                      <a href="{{route('delete.unused')}}" class="waves-effect">Sync Storage</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(deptPerModule('sop')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                        <a href="{{route('admin.sop.form.sop')}}" class="waves-effect">Add SOP</a>
                    </li>
                    <li>
                      <a href="{{route('admin.sop.delete.sop')}}" class="waves-effect">Delete SOP</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(deptPerModule('cpl')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('admin.policy.form.policy')}}" class="waves-effect">Company Policy</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(deptPerModule('nws')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('admin.form.news')}}" class="waves-effect">News</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(deptPerModule('ann')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('admin.form.announcement')}}" class="waves-effect">Announcement</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(deptPerModule('mtp')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('admin.form.tips')}}" class="waves-effect">Matrix Tips</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(array('hcl')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('admin.form.corner')}}" class="waves-effect">HC Corner</a>
                    </li>
                    @endif
                  </ul>
                </div>
            </li>
            @endif
          </ul>
        </li>
      </ul>
      <div class="sidenav-bg mask-strong"></div>
    </div>
    <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav navbar-dark" style="background-color: #151A48 !important;">
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse" style="color: white;"><i class="fas fa-bars"></i> Menu</a>
      </div>
      <ul class="nav navbar-nav nav-flex-icons ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">{{auth()->user()->name}}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{route('logout')}}">Log Out</a>
          </div>
        </li>
      </ul>
    </nav>
  </header>
