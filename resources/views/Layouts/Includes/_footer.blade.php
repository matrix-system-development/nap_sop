
  <footer class="page-footer pt-0 mt-5">

    <!-- Copyright -->
    <div class="footer-copyright py-3 text-center">
      <div class="container-fluid">
        <p>Version 1.1</p>
        <p>© {{date('Y')}} Copyright: <a href="#"> PT. NAP Info Lintas Nusa</a></p>
      </div>
    </div>
    <!-- Copyright -->

  </footer>
