
@include('Layouts.Includes.withAuth._sidebar-auth')
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('dist2/img/Matrix Black.png') }}" alt="Matrix Logo" class="brand-image img-square elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">NAP SOP</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-header">MENU</li>
                <li class="nav-item">
                    <a href={{ route('home') }} class="nav-link">
                        <i class="nav-icon fas fa-house-user"></i>
                        <p>
                        Home
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href={{ route('sop.history') }} class="nav-link">
                        <i class="nav-icon fas fa-history"></i>
                        <p>
                        History
                        </p>
                    </a>
                </li>
                @yield('sidebar-auth-html')
                <!-- <li class="nav-header">MISCELLANEOUS</li> -->
                <li class="nav-item">
                    <a href={{ url("") }} class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                        Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>