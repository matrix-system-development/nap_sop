@section('sidebar-auth-html')
@if(Auth::user()->hasAnyRole(array('admin', 'superadmin')))
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-table"></i>
        <p>
            Admin
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        @if(Auth::user()->hasRole(array('superadmin')))
        <li class="nav-item">
            <a href="{{route('add.user')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Add User</p>
            </a>
        </li>
        @endif
        @if(Auth::user()->hasAnyDept(array('rnc')) || Auth::user()->hasRole(array('superadmin')))
        <li class="nav-item">
            <a href="{{route('admin.rnc.form')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Upload File</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('viewUpdate')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Update File</p>
            </a>
        </li>
        @endif
    @if(Auth::user()->hasAnyDept(array('mrc')) || Auth::user()->hasRole(array('superadmin')))
        <li class="nav-item">
            <a href="{{route('createNews')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Create News</p>
            </a>
        </li>
    </ul>
    @endif
</li>
@endif
@endsection