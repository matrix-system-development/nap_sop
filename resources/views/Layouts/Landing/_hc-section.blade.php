@section('hc-section-html')
<div class="col-md-12"  data-aos="fade-up" data-aos-offset="300"
data-aos-delay="500" data-aos-duration="1000">
    <section class="row">
        <div class="col-md-6 col-12">
          <section class="mb-5">
              <div class="card card-cascade narrower">
                  <div class="row">
                    <div class="col-12 mr-0">
                      <div class="row">
                        <div class="col-12">
                          <section class="row">
                            <div class="col-12 col-md-6">
                              <div class="view view-cascade gradient-card-header blue-gradient">
                                <h4 class="h4-responsive mb-0 font-weight-bold">HC Corner</h4>
                              </div>
                            </div>
                          </section>
                        </div>
                      </div>
                      <div class="card-body card-body-cascade pb-0">
                        <div class="row card-body pt-3">
                              <div class="col-md-12">
                                <div class="container fluid">
                                    <livewire:components.landing.hc-section />
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <div class="row mt-2 mb-2">
                        <div class="col-md-12">
                            <div class="text-center">
                                <a class="btn bg-transparent w-30 cyan-text" href="{{route('hc-corner.all')}}">
                                    See All
                                </a>
                            </div>
                        </div>
                    </div>
              </div>
            </section>
        </div>
        <div class="col-md-6 col-12">
          <section class="mb-5">
              <div class="card card-cascade narrower">
                  <div class="row">
                    <div class="col-12 mr-0">
                      <div class="row">
                        <div class="col-12">
                          <section class="row">
                            <div class="col-12 col-md-6">
                              <div class="view view-cascade gradient-card-header blue-gradient">
                                <h4 class="h4-responsive mb-0 font-weight-bold">Matrix Tips</h4>
                              </div>
                            </div>
                          </section>
                        </div>
                      </div>
                      <div class="card-body card-body-cascade pb-0">
                        <div class="row card-body pt-3">
                              <div class="col-md-12">
                                <div class="container fluid">
                                  <livewire:components.landing.matrix-tips />
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="row mt-2 mb-2">
                    <div class="col-md-12">
                        <div class="text-center">
                            <a class="btn bg-transparent w-30 cyan-text" href="{{route('tips.all')}}">
                                See All
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </section>
        </div>
    </section>
</div>


@endsection
