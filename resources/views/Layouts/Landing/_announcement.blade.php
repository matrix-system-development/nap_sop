@section('announcement-html')
<section class="mt-md-4 pt-md-2 mb-3 aos-item" data-aos="fade-up" data-aos-offset="300"
data-aos-delay="500" data-aos-duration="1000">
    <div class="card card-cascade narrower">
        <section>
            <div class="row">
                <div class="col-12 mr-0">
                    <div class="row">
                    <div class="col-12">
                        <section class="row">
                            <div class="col-12 col-md-6">
                                <div class="view view-cascade gradient-card-header blue-gradient">
                                    <h4 class="h4-responsive mb-0 font-weight-bold">Announcement</h4>
                                </div>
                            </div>
                        </section>
                    </div>
                    </div>
                    <div class="card-body card-body-cascade pb-0">
                        <div class="container-fluid">
                            <div class="row card-body pt-1">
                                <div class="col-12">
                                    <livewire:components.landing.announcement-home />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row mt-2 mb-2">
            <div class="col-md-12">
                <div class="text-center">
                    <a class="btn bg-transparent w-50 cyan-text" href="{{route('announcement.all')}}">
                        See All Announcement
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

