@section('policy-html')
<section class="mt-md-4 pt-md-2 mb-5 aos-item" data-aos="fade-up" data-aos-offset="300"
data-aos-delay="500" data-aos-duration="1000">
    <!-- Card -->
    <div class="card card-cascade narrower">
      <!-- Section: Chart -->
      <section>
        <!-- Grid row -->
        <div class="row">
          <!-- Grid column -->
          <div class="col-sm-12 mr-0">
            <!-- Card image -->
            <div class="row">
                <div class="col-sm-12">
                  <section class="row">
                    <div class="col-sm-12 col-sm-3">
                      <div class="view view-cascade gradient-card-header blue-gradient">
                          <a href="{{route('policy.all')}}">
                              <h4 class="h4-responsive mb-0 font-weight-bold white-text">
                                  Company Policy
                              </h4>
                          </a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            <!-- Card content -->
            <div class="card-body card-body-cascade pb-0">
              <!-- Panel data -->
              <div class="row card-body pt-3">
                <!-- First column -->
                <div class="col-12">
                    @include('Layouts.Tools.landing._carouselpolicy')
                    @yield('carousel-policy-html')
                </div>
                <!-- First column -->
              </div>
              <!-- Panel data -->
            </div>
            <!-- Card content -->
          </div>
        </div>
        <div class="row mt-1 mb-2">
            <div class="col-md-12">
                <div class="text-center">
                    <a class="btn bg-transparent w-25 cyan-text" href="{{route('policy.all')}}">
                        See All
                    </a>
                </div>

            </div>
        </div>
        <!-- Grid row -->
      </section>
      <!-- Section: Chart -->
    </div>
  </section>
  <script src="{{asset('diy/js/fittext.js')}}">

  </script>
  <script>
    window.fitText( document.getElementById("carousel-item") );
  </script>
@endsection
