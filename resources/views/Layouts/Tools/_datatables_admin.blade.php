@section('datatables-html')
<table id="example2" class="table table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Department</th>
            <th>Status</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@endsection

@section('datatables-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>  
<script>
  $(document).ready(function() {
    var table = $('#example2').DataTable({
        pageLength: 2,
        processing: true,
        serverSide: true,
        paging: true,
        searching: false,
        lengthChange: false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        ajax: {
          "url" : '{{ route("adminFetchSOP") }}',
          "data" : function (d) {
            d.filter_dept = $('#filter_dept').val();
            d.filter_search = $('#filter_search').val();
          }
        },
        columns: [
            { data: 'title', name: 'title' },
            { data: 'dept_name', name: 'dept_name' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    $('#filter_dept').change(function () {
        table.draw();
    });
    $('.filter_search').keyup(function () {
        table.column( $(this).data('column'))
        .search( $(this).val() )
        .draw();
    });
  });
  
</script>
@endsection