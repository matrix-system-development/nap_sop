@section('news-html')
<div class="video-area background_one area-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7">
                <livewire:components.landing.news-home />
            </div> 
            <div class="col-lg-5">
                <livewire:components.landing.news-home-sec />
            </div>
        </div>
    </div>
</div>
@endsection