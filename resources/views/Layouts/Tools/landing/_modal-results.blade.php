@section('modal-results-html')

<div class="modal fade" id="searchResult" tabindex="-1" role="dialog" aria-labelledby="searchResult"
aria-hidden="true">
<div class="modal-dialog modal-fluid modal-dialog-scrollable modal-success mw-100 w-100" role="document">
  <!-- Content -->
  <div class="modal-content">
    <!-- Header -->
    <div class="modal-header" style="background-color: #151A48 !important;">
        <input class="form-control form-control-lg"
        type="search"
        name="search"
        id="search"
        placeholder='Search... '
        autocomplete="off"
        x-data @input.debounce.500ms ="window.livewire.emit('search-result', { search : $event.target.value})"
        >
      <button type="button" class="close" data-dismiss="modal" onclick="resetSearch()" aria-label="Close">
        <span aria-hidden="true" class="white-text">&times;</span>
      </button>
    </div>
    <!-- Body -->
    <div class="modal-body" id="body-modals">
        <livewire:components.landing.modal-search />
    </div>
    {{-- <div class="modal-footer justify-content-between">
      <button type="button" class="btn btn-warning" onclick="resetSearch()">Clear Result</button>
      <button type="button" class="btn btn-default" onclick="resetSearch()" data-dismiss="modal">Close</button>
    </div> --}}
  </div>
  <!-- Content -->
</div>
</div>
@endsection
@section('infinity-scroll-search')
<script>
    jQuery(function($) {
        $('.modal-body').on('scroll', function() {
            if ($(this).scrollTop() +
                $(this).innerHeight() >=
                document.getElementById('body-modals').scrollHeight) {
                    setTimeout(() => { window.livewire.emit('post-data'); }, 250);
            }
        });
    });
</script>
@endsection
@section('search-method')
<script type="text/javascript">
    function resetSearch() {
      document.getElementById("search").value = '';
      window.livewire.emit('reset-search');
      window.livewire.emit('$refresh');
    };
    function fetchResult() {
        var x = document.getElementById("search");
        if(x.value.length < 2) {
            window.livewire.emit('reset-search');
        }
        else {
            window.livewire.emit('search-result', { search : x.value});
        }
    };
</script>
@endsection
