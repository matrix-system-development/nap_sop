<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>NAP Info - Home</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{ asset('dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/mdb.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{asset('diy/css/carousel.css') }}">
  <link rel="stylesheet" href="{{asset('diy/css/announcement.css') }}">
  <link rel="stylesheet" href="{{asset('diy/css/news.css') }}">
  {{-- <link rel="stylesheet" href="{{asset('diy/css/carousel-row.css') }}"> --}}
  <link rel="stylesheet" href="{{asset('diy/css/highlight.css') }}">
  <script src="{{ asset('plugins/jquery/jquery.js') }}"></script>
  <script defer src="{{ asset('plugins/alpinejs/cdn.min.js') }}"></script>
  <script src="{{ asset('plugins/mark.js/mark.min.js') }}"></script>
  <script defer src="{{ asset('plugins/alpinejs/cdn.min.js') }}"></script>
  {{-- <script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script> --}}
  <script src="{{ asset('plugins/pdfjs-dist/build/pdf.js') }}"></script>
  <script src="{{ asset('plugins/pdfjs-dist/build/pdf.worker.js') }}"></script>
  <script src="{{ asset('plugins/pdfThumbnails/pdfThumbnails.js') }}"></script>
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

  @livewireScripts
  @livewireStyles
</head>
<body class="hidden-sn white-skin">
  <main style="padding-top: 1rem !important;">
    @include('Layouts.Includes._navside')
  @yield('content')
  </main>
  @include('Layouts.Tools.landing._modal-results')
  @include('Layouts.Tools.landing._search-input')
  @yield('modal-results-html')
  @yield('infinity-scroll-search')
  @yield('search-method')


  @include('Layouts.Includes._footer')
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
    AOS.init({
        useClassNames: false,
        once: false,
        mirror: true,
    });
  </script>
  <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('dist/js/popper.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('dist/js/mdb.min.js') }}"></script>
  <script src="{{ asset('diy/js/carousel.js') }}"></script>
  {{-- <script src="{{ asset('diy/js/forecast.js') }}"></script> --}}
  <script>
    $(document).ready(function() {
      $(".button-collapse").sideNav();
      var container = document.querySelector('.custom-scrollbar');
      var ps = new PerfectScrollbar(container, {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
      });
    });
  </script>
</body>
</html>
