var url_upload = document.getElementById("url_upload").getAttribute("upload-route");
  let browseFile = $('#browseFile');
  let resumable = new Resumable({
      target: url_upload,
      query:{_token:document.getElementById("url_upload").getAttribute("csrf_token")} ,
      fileType: ['pdf'],
      maxFileSize: 5242880,
      chunkSize: 1*512*512,
      simultaneousUploads:1,
      headers: {
          'Accept' : 'application/json'
      },
      testChunks: false,
      throttleProgressCallbacks: 1,
  });

  resumable.assignBrowse(browseFile[0]);

  resumable.on('fileAdded', function (file) { // trigger when file picked
      showProgress();
      resumable.upload() // to actually start uploading.
  });

  resumable.on('fileProgress', function (file) { // trigger when file progress update
      updateProgress(Math.floor(file.progress() * 100));
  });

  resumable.on('fileSuccess', function (file, response) { // trigger when file upload complete
      response = JSON.parse(response)
      $('#pdfPreview').attr('src', response.path);
      $('#urlPdf').attr('value', response.pdf_name);
      $('.card-footer').show();
  });

  resumable.on('fileError', function (file, response) { // trigger when there is any error
    var parse_response = JSON.parse(response);
    bsAlert(parse_response.message);
    //   
    //   alert(parse_response.message + ' Dev Message: to convert manually visit https://docupub.com/pdfconvert/ (see compability option select PDF 1.4)');
  });


  let progress = $('.progress');
  function showProgress() {
      progress.find('.progress-bar').css('width', '0%');
      progress.find('.progress-bar').html('0%');
      progress.find('.progress-bar').removeClass('bg-success');
      progress.show();
  }

  function updateProgress(value) {
      progress.find('.progress-bar').css('width', `${value}%`)
      progress.find('.progress-bar').html(`${value}%`)
  }

  function hideProgress() {
      progress.hide();
  }

  var bsAlert = function(message) {
    if ($("#bs-alert").length == 0) {
          $('body').append(
        '<div class="modal fade" id="bs-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
           '<div class="modal-dialog modal-notify modal-danger" role="document">'+
             '<div class="modal-content">'+
               '<div class="modal-header">'+
                 '<p class="heading">PDF Error</p>'+
                 '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="white-text">&times;</span></button>'+
               '</div>'+
               '<div class="modal-body">'+
                 '<div class="row">'+
                    '<div class="col-12">'+
                        '<p>'+message+'</p>'+
                        '<p>'+'Dev Message: to convert manually visit '+'<a href="https://docupub.com/pdfconvert/">https://docupub.com/pdfconvert/</a> '+  '(see compability option select PDF 1.4)'+'</p>'+
                    '</div>'+
                 '</div>'+
               '</div>'+
               '<div class="modal-footer">'+
                 '<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>'+
               '</div>'+
             '</div>'+
           '</div>'+
         '</div>')
     } else {
         $("#bs-alert .modal-body").text(message);
     }    
     $("#bs-alert").modal();
 }

 $(document).on('hidden.bs.modal', '#bs-alert', function (event) {
  location.reload();
  });
