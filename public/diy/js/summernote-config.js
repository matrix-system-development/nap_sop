$(function () {
    $('#summernote').summernote({
      tabsize: 3,
      height: 500,
      focus: true,
      dialogsInBody: true,
      toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline','italic', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']],
          ['view', ['fullscreen', 'codeview', 'help']]
      ],
      styleTags: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
      lineHeights: ['1.0', '1.2', '1.4', '1.5', '1.6', '1.8', '2.0', '3.0'],
      maximumImageFileSize: 2097152, 
      callbacks: {
        onChange: function(contents, $editable) {
          window.livewire.emit('refresh-contents', { content : contents});
        },
        onImageUpload : function(files) {
      if (!files.length) return;
      var file = files[0];
      // create FileReader
      var reader  = new FileReader();
      reader.onloadend = function () {
          console.log("img",$("<img>"));
          var img = $("<img>").attr({src: reader.result, width: "100%"}); // << Add here img attributes !
          console.log("var img", img);
          $(summernote).summernote("insertNode", img[0]);
      }

      if (file) {
        // convert fileObject to datauri
        reader.readAsDataURL(file);
      }
  }
      },
      cleaner:{
          action: 'both', 
          newline: '<br>', 
          icon: '<i class="note-icon">[Your Button]</i>',
          keepHtml: false,
          keepOnlyTags: ['<p>', '<br>', '<ul>', '<li>', '<b>', '<strong>','<i>', '<a>'], 
          keepClasses: false,
          badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'],
          badAttributes: ['style', 'start'],
          limitChars: false, 
          limitDisplay: 'both',
          limitStop: false
      }
    })
  })
  function save() {
    var x = document.getElementById("summernote").value;
    console.log('onClick:', x);
    // window.livewire.emit('refresh-contents', { content : x});
  };