<?php
use App\Models\Posts;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;
use App\Http\Controllers\BreadcrumbsController;

Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for('add.user', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Add User', route('add.user'));
});
Breadcrumbs::for('update.user', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Update User Password', route('update.user'));
});
Breadcrumbs::for('delete.unused', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Delete Unused File', route('delete.unused'));
});

Breadcrumbs::for('view.activity', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Activity', route('view.activity'));
});

Breadcrumbs::for('sop.all', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('SOP', route('sop.all'));
});

Breadcrumbs::for('policy.all', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Policy', route('policy.all'));
});

Breadcrumbs::for('sop.title', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('sop.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});

Breadcrumbs::for('policy.title', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('policy.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});

Breadcrumbs::for('sop.id.page', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('sop.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});

Breadcrumbs::for('policy.id.page', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('policy.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});



Breadcrumbs::for('news.all', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    // $trail->push('News', route('news.all'));
    $trail->push('News', route('news.all'));
});
Breadcrumbs::for('hc-corner.all', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('HC Corner', route('hc-corner.all'));
});
Breadcrumbs::for('hc-corner.title', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('hc-corner.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});
Breadcrumbs::for('tips.all', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Matrix Tips', route('tips.all'));
});
Breadcrumbs::for('tips.title', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('tips.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});

Breadcrumbs::for('announcement.all', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Announcement',route('announcement.all'));
});
Breadcrumbs::for('announcement.title', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('announcement.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});

Breadcrumbs::for('news.title', function (BreadcrumbTrail $trail, $parameter) {
    $trail->parent('news.all');
    $trail->push((new BreadcrumbsController)->getTitle($parameter));
});

//admin starts

Breadcrumbs::for('admin.sop.form.sop', function (BreadcrumbTrail $trail) {
    $trail->parent('sop.all');
    $trail->push('Upload SOP', route('admin.sop.form.sop'));
});
Breadcrumbs::for('admin.sop.delete.sop', function (BreadcrumbTrail $trail) {
    $trail->parent('sop.all');
    $trail->push('Delete SOP', route('admin.sop.delete.sop'));
});
Breadcrumbs::for('admin.policy.form.policy', function (BreadcrumbTrail $trail) {
    $trail->parent('policy.all');
    $trail->push('Upload Company Policy', route('admin.policy.form.policy'));
});

Breadcrumbs::for('admin.form.news', function (BreadcrumbTrail $trail) {
    $trail->parent('news.all');
    $trail->push('Create News', route('admin.form.news'));
});

Breadcrumbs::for('admin.form.announcement', function (BreadcrumbTrail $trail) {
    $trail->parent('announcement.all');
    $trail->push('Create Announcement', route('admin.form.announcement'));
});

Breadcrumbs::for('admin.form.tips', function (BreadcrumbTrail $trail) {
    $trail->parent('tips.all');
    $trail->push('Create Matrix Tips', route('admin.form.tips'));
});
Breadcrumbs::for('admin.form.corner', function (BreadcrumbTrail $trail) {
    $trail->parent('hc-corner.all');
    $trail->push('Create HC Corner', route('admin.form.corner'));
});

