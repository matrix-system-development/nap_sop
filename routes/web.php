<?php
use App\Http\Controllers\{
    HomeController,AdminController, FileController,
    AdminViewController, HistoryController,
    AuthController,ActivityLoggerController,
    ViewController,
};
use App\Http\Controllers\withAuth\{
    SopController,NewsController,AnnouncementController,
    HcController,
    MatrixTipsController,
    PolicyController,
};
use App\Http\Controllers\withAuth\Superadmin\{
    RegistrationController,ActivityLogController,
    DeleteFileController,UpdatePasswordController,
};
use Illuminate\Support\Facades\Route;
use Stevebauman\Location\Facades\Location;

Route::get('/', function () {
    return redirect('login');
});

Route::get('/login/{q?}', [AuthController::class, 'login'])->name('login');
Route::post('/dologin', [AuthController::class, 'doLogin'])->name('doLogin');
Route::get('/logout', [AuthController::class, 'doLogout'])->name('logout');
Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::prefix('view')->group(function () {
    Route::group(['prefix' => 'sop', 'as' => 'sop.'],function () {
        Route::get('/', [FileController::class, 'viewSOPAll'])->name('all');
        Route::middleware('private')->group(function () {
            Route::get('{title}', [ViewController::class, 'viewSOP'])->name('title');
            Route::get('{title}/{page}', [ViewController::class, 'viewIdPage'])->name('id.page');
        });

    });


    Route::group(['prefix' => 'news', 'as' => 'news.'],function () {
        Route::get('/', [ViewController::class, 'viewNewsAll'])->name('all');
        Route::get('{title}', [ViewController::class, 'viewNews'])->name('title');
        Route::get('user/{string}', [ViewController::class, 'newsByUser'])->name('newsByUser');
    });
    Route::group(['prefix' => 'announcement', 'as' => 'announcement.'],function () {
        Route::get('/', [ViewController::class, 'viewAnnouncementAll'])->name('all');
        Route::get('{title}', [ViewController::class, 'viewAnnouncement'])->name('title');
    });
    Route::group(['prefix' => 'hc-corner', 'as' => 'hc-corner.'],function () {
        Route::get('/', [ViewController::class, 'viewHCornerAll'])->name('all');
        Route::get('{title}', [ViewController::class, 'viewHCorner'])->name('title');
    });
    Route::group(['prefix' => 'tips', 'as' => 'tips.'],function () {
        Route::get('/', [ViewController::class, 'viewTipsAll'])->name('all');
        Route::get('{title}', [ViewController::class, 'viewTips'])->name('title');
    });
    Route::group(['prefix' => 'policy', 'as' => 'policy.'],function () {
        Route::get('/', [FileController::class, 'viewPolicyAll'])->name('all');
        Route::get('{title}', [ViewController::class, 'viewPolicy'])->name('title');
        Route::get('{title}/{page}', [ViewController::class, 'viewIdPagePolicy'])->name('id.page');
    });
});
Route::get('/sop/dept/{id}', [FileController::class, 'viewSOPDept'])->name('view.sop.dept');

Route::get('/sop/history', [HistoryController::class, 'view'])->name('sop.history');
Route::get('/view/isolate/{string}/{additional?}', [FileController::class, 'isolate']);
Route::get('/view/isolatethumbnail/{string}', [FileController::class, 'isolateThumbnail']);

Route::group(['namespace' => 'withAuth'], function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.'],function () {
        Route::middleware('dept:rnc')->group(function () {
            Route::group(['prefix' => 'sop', 'as' => 'sop.'],function () {
                    Route::get('upload', [SopController::class, 'uploadView'])->name('form.sop');
                    Route::get('delete', [SopController::class, 'deleteView'])->name('delete.sop');
                    Route::post('upload', [SopController::class, 'fileUpload'])->name('uploadSOP');
                    Route::post('uploadsplit', [SopController::class, 'uploadSplitSOP'])->name('uploadSplitSOP');
            });
            Route::group(['prefix' => 'policy', 'as' => 'policy.'],function () {
                Route::get('upload', [PolicyController::class, 'uploadView'])->name('form.policy');
                Route::post('upload', [PolicyController::class, 'fileUpload'])->name('uploadPolicy');
                Route::post('uploadsplit', [PolicyController::class, 'uploadSplitPolicy'])->name('uploadSplitPolicy');
            });
        });
        Route::middleware('dept:mrc,hcl')->group(function () {
            Route::get('announcement/create', [AnnouncementController::class, 'create'])->name('form.announcement');
        });
        Route::middleware('dept:mrc')->group(function () {
            Route::get('news/create', [NewsController::class, 'createNews'])->name('form.news');
            Route::get('tips/create', [MatrixTipsController::class, 'createTips'])->name('form.tips');
        });
        Route::middleware('dept:hcl')->group(function () {
            Route::get('corner/create', [HcController::class, 'createCorner'])->name('form.corner');
        });
    });
});
Route::group(['namespace' => 'withAuth/Superadmin'], function () {
    Route::get('superadmin/add/user', [RegistrationController::class, 'add'])->name('add.user');
    Route::get('superadmin/update/user/password', [UpdatePasswordController::class, 'update'])->name('update.user');
    Route::get('superadmin/log/activity', [ActivityLogController::class, 'view'])->name('view.activity');
    Route::get('superadmin/delete/unused-file', [DeleteFileController::class, 'ViewDeleteUnusedFile'])->name('delete.unused');
    Route::get('superadmin/delete/unused-file/process', [DeleteFileController::class, 'ProcessDeleteUnusedFile'])->name('delete.unused.process');
});

Route::get('/dummy', [HomeController::class, 'dummy']);
Route::post('/activity-logger', [ActivityLoggerController::class, 'logging'])->name('logger');

