<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('depts')->insert([
            'dept_code' => 'rnc',
            'dept_name' => 'Risk and Compliance',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'mrc',
            'dept_name' => 'Marketing Communication',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'syd',
            'dept_name' => 'System Development',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'hcl',
            'dept_name' => 'Human Capital',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'cse',
            'dept_name' => 'Customer Care',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'sas',
            'dept_name' => 'Sales Support',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'ens',
            'dept_name' => 'Enterprise Sales',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'whs',
            'dept_name' => 'Wholesales Sales',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'kas',
            'dept_name' => 'Key Account Sales',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'brs',
            'dept_name' => 'Broadband Sales',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'gvs',
            'dept_name' => 'Government Sales',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'ant',
            'dept_name' => 'Accounting & Tax',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'fin',
            'dept_name' => 'Finance',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'bcl',
            'dept_name' => 'Budget Control',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'rae',
            'dept_name' => 'Revenue Assurance',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'pnp',
            'dept_name' => 'Property & Partnership',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'prc',
            'dept_name' => 'Procurement',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'fam',
            'dept_name' => 'Facility Asset & Management',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'sya',
            'dept_name' => 'IT & System Administrator',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'ned',
            'dept_name' => 'Network Development',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'neo',
            'dept_name' => 'Network Operations',
        ]);//   
        \DB::table('depts')->insert([
            'dept_code' => 'ney',
            'dept_name' => 'Network Delivery',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'nei',
            'dept_name' => 'Network Quality Improvement',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'dac',
            'dept_name' => 'Data Center',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'cls',
            'dept_name' => 'Cable Landing Station',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'lgl',
            'dept_name' => 'Legal',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'crp',
            'dept_name' => 'Corporate Planning',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'ina',
            'dept_name' => 'Internal Audit',
        ]);//
    }
}
