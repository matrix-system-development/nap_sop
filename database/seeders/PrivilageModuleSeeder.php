<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PrivilageModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('privilage_modules')->insert([
            'privilage_code' => 'sop',
            'privilage_name' => 'SOP',
        ]);//
        \DB::table('privilage_modules')->insert([
            'privilage_code' => 'ann',
            'privilage_name' => 'Announcement',
        ]);//
        \DB::table('privilage_modules')->insert([
            'privilage_code' => 'nws',
            'privilage_name' => 'News',
        ]);//
        \DB::table('privilage_modules')->insert([
            'privilage_code' => 'hcr',
            'privilage_name' => 'HC Corner',
        ]);//
        \DB::table('privilage_modules')->insert([
            'privilage_code' => 'mtp',
            'privilage_name' => 'Matrix Tips',
        ]);//
    }
}
