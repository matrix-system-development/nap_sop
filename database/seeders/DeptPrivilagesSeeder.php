<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DeptPrivilagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('dept_privilages')->insert([
            'id_dept' => 1,
            'id_privilage' => 1,
        ]);//
        \DB::table('dept_privilages')->insert([
            'id_dept' => 2,
            'id_privilage' => 2,
        ]);//
        \DB::table('dept_privilages')->insert([
            'id_dept' => 4,
            'id_privilage' => 2,
        ]);//
        \DB::table('dept_privilages')->insert([
            'id_dept' => 2,
            'id_privilage' => 3,
        ]);//
        \DB::table('dept_privilages')->insert([
            'id_dept' => 4,
            'id_privilage' => 4,
        ]);//
        \DB::table('dept_privilages')->insert([
            'id_dept' => 2,
            'id_privilage' => 5,
        ]);//
        
    }
}
