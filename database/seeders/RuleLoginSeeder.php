<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RuleLoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('rule_logins')->insert([
            'rule_name' => 'sso',
            'rule_value' => '1',
        ]);//
        \DB::table('rule_logins')->insert([
            'rule_name' => 'sso logout',
            'rule_value' => 'https://intranet.nap.net.id/sso/',
        ]);//
    }
}
