<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolicyPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_pages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_policy')->unsigned();
            $table->foreign('id_policy')->references('id')->on('file_policy')->onDelete('cascade');
            $table->string('page');
            $table->text('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdf_pages');
    }
}
