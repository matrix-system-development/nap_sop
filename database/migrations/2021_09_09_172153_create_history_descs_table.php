<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryDescsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_descs', function (Blueprint $table) {
            $table->id();
            $table->string('desc');
            $table->bigInteger('id_history_sop')->unsigned();
            $table->bigInteger('id_history_policy')->unsigned();
            $table->foreign('id_history_sop')->references('id')->on('history_sop')->onDelete('cascade');
            $table->foreign('id_history_policy')->references('id')->on('history_policy')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_descs');
    }
}
