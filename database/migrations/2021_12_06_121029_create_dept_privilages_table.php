<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeptPrivilagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dept_privilages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_dept')->unsigned();
            $table->bigInteger('id_privilage')->unsigned();
            $table->foreign('id_dept')->references('id')->on('depts')->onDelete('cascade');
            $table->foreign('id_privilage')->references('id')->on('privilage_modules')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dept_privilages');
    }
}
