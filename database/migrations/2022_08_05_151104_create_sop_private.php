<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSopPrivate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sop_private', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_sop')->unsigned();
            $table->bigInteger('id_dept')->unsigned();
            $table->foreign('id_sop')->references('id')->on('sop_pdf')->onDelete('cascade');
            $table->foreign('id_dept')->references('id')->on('depts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sop_private');
    }
}
