<?php

use Illuminate\Support\Facades\Auth;

if(!function_exists('string_seo')) {
    function string_seo_helper($string)
    {
        if (strlen($string) > 5) {
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, 5));
        }
        $string = str_replace(array('[\', \']'), '', $string);
        $string = preg_replace('/\[.*\]/U', '', $string);
        $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
        $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
        return strtolower(trim($string, '-'));
    }
}


