<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\DeptPrivilage;

function deptPrivilage(){
    $dept_priv = DeptPrivilage::get();
    return $dept_priv;
}
function deptPerModule($param){
    try {
        $dept_priv = DeptPrivilage::
        join('privilage_modules', 'privilage_modules.id', '=', 'dept_privilages.id_privilage')
        ->join('depts', 'depts.id', '=', 'dept_privilages.id_dept')
        ->where('privilage_modules.privilage_code', '=', $param)
        ->pluck('depts.dept_code')
        ->toArray();
        return $dept_priv;
    } catch (\Exception  $e) {
        return $dept_priv = ["null"];
    }

}
function deptPerPrivilage($param){
    try {
        $dept_priv = DeptPrivilage::
        join('privilage_modules', 'privilage_modules.id', '=', 'dept_privilages.id_privilage')
        ->join('depts', 'depts.id', '=', 'dept_privilages.id_dept')
        ->where('depts.dept_code', '=', $param)
        ->pluck('depts.dept_code')
        ->toArray();
        return $dept_priv;
    } catch (\Exception  $e) {
        return $dept_priv = ["null"];
    }
}


