<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\{
    Posts, Dept, 
};
class PolicyPaginate extends Component
{
    public $selected_id = null;
    public $sop_dept;
    use WithPagination;
    public $searchTerm;
    public $sortParam, $sortDir;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $searchTerm = '%'.$this->searchTerm.'%';

        $query= Posts::join('file_policy', 'file_policy.id_post', '=','posts.id')
        ->where('posts.title', 'like', $searchTerm)
        ->orderBy('posts.'.(!is_null($this->sortParam) ? $this->sortParam : 'title'), 
        (!is_null($this->sortDir) ? $this->sortDir : 'ASC'))
        ->paginate(3);
        $this->dispatchBrowserEvent('contentChanged');
        return view('livewire.policy-paginate', compact('query'));
    }
    public function getQueryString()
    {
        return [];
    }
}
