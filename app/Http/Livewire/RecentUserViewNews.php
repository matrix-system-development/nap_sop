<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\{Posts,ActivityLog};
use Illuminate\Support\Facades\Auth;

class RecentUserViewNews extends Component
{
    public function render()
    {
        try
        {
            $log = \DB::table('activity_log')
            ->where('causer_id', Auth::id())
            ->where('log_name', 'VIEW_NEWS')
            ->orderBy('created_at', 'desc')
            ->get()
            ->unique('subject_id')
            ->pluck('subject_id');
    
            $query = Posts::join('news as nw', 'posts.id', '=', 'nw.id_post')
            ->whereIn('nw.id', $log)
            ->take(3)
            ->get();
        }
        catch(\Exception $e)
        {
            $query = [];
        }

        return view('livewire.recent-user-view-news', ['recents' => $query]);
    }
}
