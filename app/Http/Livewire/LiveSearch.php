<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Posts;

class LiveSearch extends Component
{
    use WithPagination;

    public $sortField = 'title'; // default sorting field
    public $sortAsc = true; // default sort direction
    public $search, $isEmpty;
    public $limitPerPage = 5;
    protected $listeners = [
        'reset-search' => 'resetFilters',
        'post-data' => 'postData',

    ];
    public function postData()
    {
        $this->limitPerPage = $this->limitPerPage + 2;
    }

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function resetFilters()
    {
        $this->reset('search');
        $contents = null;
    }
    public function render()
    {
        $contents = null;
        $tables = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('sp.status', 'active')
        ->where('title','LIKE', '%'.$this->search.'%')
        ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
        ->simplePaginate(10);
        if (!is_null($this->search)){
            $contents = Posts::join('depts', 'posts.id_dept', '=', 'depts.id')
            // ->join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
            ->join('pdf_pages as pp', 'sp.id', '=', 'pp.id_pdf')
            ->where('sp.status', 'active')
            ->where('pp.content','LIKE', '%'.$this->search.'%')
            ->paginate($this->limitPerPage);
        }else {
            $this->reset('search');
            $contents = null;
        }
        $this->emit('postStore', '');

        return view('livewire.live-search', [
            'tables' => $tables,
            'contents' => $contents,
        ]);

    }

}
