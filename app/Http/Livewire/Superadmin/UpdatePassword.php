<?php

namespace App\Http\Livewire\Superadmin;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Traits\{LoggerTrait};

use App\Models\{
    User,Role, Dept,
    RelUserDept,
    RelUserRole, RelUserId,
};

class UpdatePassword extends Component
{
    use LoggerTrait;
    public $query= '';
    public array $accounts = [];
    public int $selectedAccount = 0;
    public int $highlightIndex = 0;
    public bool $showDropdown;

    public $inputEmail, $password;
    protected $rules = [
        'query' => 'required|email',
        'password' => 'required',

    ];
    public function mount()
    {
        $this->reset();
    }
    
    public function reset(...$properties)
    {
        $this->accounts = [];
        $this->highlightIndex = 0;
        $this->query = '';
        $this->selectedAccount = 0;
        $this->showDropdown = true;
    }

    public function hideDropdown()
    {
        $this->showDropdown = false;
    }

    public function incrementHighlight()
    {
        if ($this->highlightIndex === count($this->accounts) - 1) {
            $this->highlightIndex = 0;
            return;
        }

        $this->highlightIndex++;
    }

    public function decrementHighlight()
    {
        if ($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->accounts) - 1;
            return;
        }

        $this->highlightIndex--;
    }

    public function selectAccount($id = null)
    {
        $id = $id ?: $this->highlightIndex;

        $account = $this->accounts[$id] ?? null;

        if ($account) {
            $this->showDropdown = true;
            $this->query = $account['email'];
            $this->selectedAccount = $account['id'];
        }
    }

    public function updatedQuery()
    {
        $this->accounts = User::where('email', 'like', '%' . $this->query. '%')
            ->take(5)
            ->get()
            ->toArray();
    }
    public function submit()
    {
        $this->validate();
        $findUser = User::where('email', $this->query)->firstOrFail();
        if($findUser) {
            $newPassword = Hash::make($this->password);
            $updatePassword = DB::table('users')->where('email', $this->query)->update(['password' => $newPassword]);
            return redirect(request()->header('Referer'))->with('success', 'User Has Been Updated');
        }
    }

    public function render()
    {
        return view('livewire.superadmin.update-password');
    }
}
