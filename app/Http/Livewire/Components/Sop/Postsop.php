<?php

namespace App\Http\Livewire\Components\Sop;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\{TextProcessingTrait,LoggerTrait};
use App\Models\{
    Posts, File_sop, Dept,
    HistorySOP, History_desc,PDFDesc,
    PDFPage,PostUserRel,
};

class Postsop extends Component
{
    use WithFileUploads,TextProcessingTrait,LoggerTrait;
    public $depts, $title, $dept,$file,$file2,
            $selected_id, $check;
    public $converted = null;
    public $updateMode = false;
    protected $listeners = [
        'get-converted' => 'convertGet',
    ];
    protected $rules = [
        'file' => 'required|mimes:pdf,doc,docx|max:50000',
        'title' => 'required|string|max:40',
        'dept' => 'required|exists:depts,dept_name',
    ];
    public function updatedFileUpload()
    {
        $this->validate([
            'file' => 'required|mimes:pdf,doc,docx|max:50000',
        ]);
    }
    // public function updatedFile()
    // {
    //     if($this->validateOnly('file'))
    //     {
    //         $this->file2 = $this->file;
    //     }
    //     else{
    //         $this->file2 = null;
    //     }
    // }
    public function mount()
    {
        $this->depts = Dept::get();
    }
    public function convertGet($term)
    {
        $this->check = $term['check'];
        $this->converted = $term['filename'];
    }
    private function resetInput()
    {
        $this->title = null;
        $this->dept = null;
        $this->file = null;
        $this->file2 = null;
    }
    public function edit($id)
    {
        $query = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('posts.id',$id)
        ->firstOrFail();

        $this->selected_id = $id;
        $this->title = $query->title;
        $this->dept = $query->dept_name;
        $this->updateMode = true;
    }
    public function submit()
    {
        // dd($this->dept);
        $this->validate();
        $fileModel = new File_sop;
        $postModel = new Posts;
        $history = new HistorySOP;
        $history_desc = new History_desc;
        $pdf_descs = new PDFDesc;
        $user_post = new PostUserRel;
        $parser = new \Smalot\PdfParser\Parser();
        $count_page = 1;
        DB::transaction(function () use (
            $postModel, $fileModel, $history,
            $history_desc,$pdf_descs,
            $parser,$count_page,$user_post)
            {
                $findDept = Dept::where('dept_name',$this->dept)
                ->firstOrFail('id');
                $postModel->title = $this->title;
                $postModel->id_dept = $findDept->id;
                $postModel->seo_url = $this->seo_friendly_url($this->limit_text($this->title, 4)).'-'.$findDept->id.time();
                // $dir_path = $postModel->id_dept.'-'.$this->seo_friendly_url($this->limit_text($this->title, 4)).'-'.time();
                if(in_array($this->file->getClientOriginalExtension(), array('docx','doc')))
                {
                    $fileModel->filename = time().'_'.
                    preg_replace('/\s+/', '_', $this->title).'.pdf';
                    // dd($this->converted);
                    File::copy(public_path('storage/uploads/livewire-tmp/'.$this->converted),public_path('storage/uploads/'.$fileModel->filename));
                    $fileModel->file_path = '/storage/uploads/'.$fileModel->filename;
                }
                else
                {
                    $fileModel->filename = time().'_'.
                    preg_replace('/\s+/', '_', $this->title).'.'.
                    $this->file->getClientOriginalExtension();
                    $filePath = $this->file->storeAs('uploads/sop/'.$postModel->seo_url, $fileModel->filename, 'public');
                    $fileModel->file_path = '/storage/' . $filePath;
                }
                $postModel->save();
                $fileModel->status = 'active';
                $fileModel->id_post = $postModel->id;
                $fileModel->save();
                $history->id_post = $postModel->id;
                $history->save();
                $history_desc->id_history_sop = $history->id;
                $history_desc->desc = 'first upload';
                $history_desc->save();

                $parserPdf = $parser->parseFile(storage_path().'\app\public/'.trim($fileModel->file_path,"/storage"));
                $pdf_descs->id_post = $fileModel->id;
                $pdf_descs->author = isset($parserPdf->getDetails()['Author']) ? $parserPdf->getDetails()['Author'] : null;
                $pdf_descs->creator = isset($parserPdf->getDetails()['Creator']) ? $parserPdf->getDetails()['Creator'] : null;
                $pdf_descs->pages = $parserPdf->getDetails()['Pages'];
                $pdf_descs->save();

                $data_page = [];
                $forLoop = $parserPdf->getPages();
                foreach ($forLoop as $page) {
                    $pdf_page = new PDFPage;
                    $pdf_page->id_post = $fileModel->id;
                    $pdf_page->page = $count_page;
                    $pdf_page->content =  preg_replace('/\s+/', ' ',$page->getText());
                    $data_page[] = $pdf_page->attributesToArray();
                    $count_page++;
                }
                $user_post->id_post = $postModel->id;
                $user_post->id_user = auth()->user()->id;
                $user_post->name_user = auth()->user()->name;
                PDFPage::insert($data_page);
            $this->loggingActivity(['name'=> 'UPLOAD_SOP', 'subject_id'=> $fileModel, 'description' => 'Upload SOP - '. $postModel->title]);
            $this->resetInput();
            return redirect()->route('sop.title', ['title' => $postModel->seo_url])->with('success', 'Berhasil Upload SOP Baru  - '.$postModel->title);
            // return redirect(request()->header('Referer'))->with('success', 'Berhasil Upload SOP Baru - '.$postModel->title);
        });

    }
    public function render()
    {
        return view('livewire.Admin.components.postsop');
    }
    public function getUpload()
    {
        dd($this->file->getClientOriginalName());
    }
}
