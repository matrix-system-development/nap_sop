<?php

namespace App\Http\Livewire\Components\Sop;

use Livewire\Component;
use Livewire\WithFileUploads;
use DomPDF;

class PreviewPdf extends Component
{
    use WithFileUploads;
    public $previewFile,$temp_url, $testt;
    public $convertedFile;
    public $convertedname = null;
    public function render()
    {
        if(in_array($this->previewFile->getClientOriginalExtension(), array('docx','doc'))){
            $this->convert();
        }
        return view('livewire.preview-pdf');
    }
    public function convert() {
        $tempname = preg_replace('/([?]expires).*/', '', basename($this->previewFile->temporaryUrl()));
        $domPdfPath = base_path('vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
        $convert = \PhpOffice\PhpWord\IOFactory::load(public_path('storage/uploads/livewire-tmp/'.$tempname)); 
        $PDFWriter = \PhpOffice\PhpWord\IOFactory::createWriter($convert,'PDF');
        $rmformat = preg_replace('/([.]docx|doc).*/', '.pdf',$tempname);
        $PDFWriter->save(public_path('storage/uploads/livewire-tmp/'.$rmformat));
        $this->convertedFile  = url('storage/uploads/livewire-tmp/'.$rmformat);
        $this->convertedname = $rmformat;
    }
}
