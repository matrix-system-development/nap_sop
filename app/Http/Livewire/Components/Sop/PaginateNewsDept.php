<?php

namespace App\Http\Livewire\Components\Sop;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use App\Models\{
    Posts, File_sop, Dept
};

class PaginateNewsDept extends Component
{
    
    use WithPagination;
    public function render()
    {
        $query = Posts::join('depts', 'posts.id_dept', '=', 'depts.id')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('depts.id',$this->id)
        ->orderBy('posts.created_at', 'desc')
        ->paginate(10);
        return view('livewire.components.sop.paginate-news-dept', ['sops' =>$this->id]);
    }
}
