<?php

namespace App\Http\Livewire\Components\Sop;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\{TextProcessingTrait,LoggerTrait};
use App\Models\{
    Posts, File_sop, Dept,
    HistorySOP, History_desc,PDFDesc,
    PDFPage,PostUserRel,
};

class DeleteSop extends Component
{
    public $searchSOP;
    public $selected =[];
    public $selected_delete = null;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $sop = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->leftJoin('depts', 'posts.id_dept', '=', 'depts.id')
        ->select('posts.id','posts.title', 'posts.seo_url', 'posts.created_at', 'depts.dept_name', 'sp.file_path')
        ->where('title','LIKE', '%'.$this->searchSOP.'%')
        ->where('sp.status', 'active')
        ->paginate(5);
        // $sop->paginate(5);
        // ->orderByRaw("FIELD(id, $selected_ordered)")
        // // ->orderByRaw('posts.id', $this->selected, 'desc')
        // // ->orderBy('posts.created_at', 'desc')
        // // ->latest()
        // ->paginate(5);
        // $temp_selected = $this->selected;
        // $test2 = $sop->sortBy(function($order) use($temp_selected){
        //     return array_search($order->id, $temp_selected);
        //  })->values()->all();
        return view('livewire.Admin.components.deletesop', ['sop' => $sop]);
    }

    public function selectDelete($id)
    {
        $this->selected_delete = $id;
        // dd($this->selected_delete);
    }

    public function getQueryString()
    {
        return [];
    }
}
