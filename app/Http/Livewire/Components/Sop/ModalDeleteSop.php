<?php

namespace App\Http\Livewire\Components\Sop;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\{TextProcessingTrait,LoggerTrait};
use App\Models\{
    Posts, File_sop, Dept,
    HistorySOP, History_desc,PDFDesc,
    PDFPage,PostUserRel,
};

class ModalDeleteSop extends Component
{
    public $selected_sop;

    protected $listeners = [
        'details-delete-show' => 'updateModalDelete',
        'confirm-delete' => 'ConfirmDelete',
        'render-details' => 'render'
    ];


    public function updateModalDelete($id)
    {
        $this->selected_sop  = $id;
        $this->dispatchBrowserEvent('render-details');
    }

    public function ConfirmDelete()
    {
        // return redirect()->route('home');
        // session()->flash('error-delete', 'Post successfully updated.');
        try {

            $find = Posts::join('sop_pdf', 'posts.id', '=', 'sop_pdf.id_post')
            ->where('posts.id', $this->selected_sop)->firstOrFail();
            if($find) {
                File_sop::where('id',$find['id'])->update(['status'=>'inactive']);
                return redirect()->route('admin.sop.delete.sop')->with('success-delete', 'Success deleting File');
                // return session()->flash('success-delete', 'Success deleting File');
            }
            return $find;
        } catch (\Exception $e) {
            return session()->flash('error-delete', 'Error deleting File');
        }
    }

    public function render()
    {
        // $find =
        return view('livewire.Admin.components.modal-delete-sop');
    }
}
