<?php

namespace App\Http\Livewire\Components\News;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\{Posts,News,PostUserRel};

class NewsUser extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $by_user;
    public function render()
    {
        // dd(explode('-',$this->by_user))
        // dd(preg_replace('/-/',' ', $this->by_user));
        // dd(Posts::join('post_user_rels', 'posts.id', 'post_user_rels.id_post')
        // ->join('news', 'news.id', '=', 'post_user_rels.id_user')
        // ->where('post_user_rels.name_user', 'like', '%'.preg_replace('/-/',' ', $this->by_user).'%')
        // ->paginate(5));
        return view('livewire.components.news.news-user', [
            'posts' => Posts::join('post_user_rels', 'posts.id', 'post_user_rels.id_post')
            ->join('news', 'news.id', '=', 'post_user_rels.id_user')
            ->where('post_user_rels.name_user', 'like', '%'.preg_replace('/-/',' ', $this->by_user).'%')
            ->paginate(5),

        ]);

        // $Posts::join('post_user_rels', 'posts.id', 'post_user_rels.id_post')
        // ->join('news', 'news.id', '=', 'post_user_rels.id_user')
        // ->where('post_user_rels.name_user', 'like', '%'.$this->by_user.'%')
        // ->paginate(10));
        return view('livewire.components.news.news-user', compact('posts'));
    }
}
