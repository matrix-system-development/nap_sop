<?php

namespace App\Http\Livewire\Components\News;

use Livewire\Component;
use Livewire\WithFileUploads;
use DomPDF;

class PreviewNewsThumbnail extends Component
{
    use WithFileUploads;
    public $previewThumbnailUrl;
    public function render()
    {
        return view('livewire.preview-thumbnail');
    }
}
