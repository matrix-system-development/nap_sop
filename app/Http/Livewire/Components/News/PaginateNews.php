<?php

namespace App\Http\Livewire\Components\News;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\{
    Posts, Dept, 
};

class PaginateNews extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sortParam, $sortDir;
    public function getQueryString()
    {
        return [];
    }

    public function render()
    {
        $news = Posts::select('posts.title', 'posts.seo_url', 'posts.created_at', 'news.thumbnail_path')
        ->join('news', 'news.id_post', '=','posts.id')
        ->orderBy('posts.'.(!is_null($this->sortParam) ? $this->sortParam : 'title'), 
            (!is_null($this->sortDir) ? $this->sortDir : 'ASC'))
        ->paginate(2);
        return view('livewire.components.news.paginate-news', compact('news'));
    }
}
