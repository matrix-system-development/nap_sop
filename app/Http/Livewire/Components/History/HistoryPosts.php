<?php

namespace App\Http\Livewire\Components\History;

use Livewire\Component;
use App\Models\{
    Posts, File_sop, Dept,
    HistorySOP, History_desc,PDFDesc,
    PDFPage,
};

class HistoryPosts extends Component
{
    public $limitPerPage = 10;
    public $newPosts = 'Created a new Post of SOP';
    public $updPosts = 'Updated a SOP';
    public function render()
    {
        $query = File_sop::join('posts', 'posts.id', '=', 'sop_pdf.id_post')
        ->Join('history_sop', 'history_sop.id_post', '=', 'posts.id')
        ->join('history_descs', 'history_descs.id_history_sop', '=', 'history_sop.id')
        ->orderBy('history_descs.created_at', 'desc')
        ->paginate($this->limitPerPage);

        // dd($query);

        return view('livewire.history-posts', ['history_content' => $query->unique()]);
    }

    public function postData()
    {
        $this->limitPerPage = $this->limitPerPage + 2;
    }
}
