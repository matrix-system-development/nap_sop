<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Collection;
use App\Models\Posts;

class ModalSearch extends Component
{
    use WithPagination;

    public $search;
    public $contents;
    public $selectedContent = ['sop', 'policy'];
    public $limitPerPage = 6;
    protected $listeners = [
        'reset-search' => 'resetFilters',
        'search-result' => 'searchResults',
        'post-data' => 'postData',
        '$refresh',
    ];

    // public function mount()
    // {
    //     $this->selectedContent = ['sop'];
    // }

    public function selectedContentFunction()
    {

    }

    public function resetFilters()
    {
        $this->reset('search','contents');
        $this->reset();
    }
    public function searchResults($term)
    {
        $this->search= $term['search'];
    }
    public function postData()
    {
        $this->limitPerPage = $this->limitPerPage + 3;
    }

    public function queryResults($terms)
    {
        if(in_array('sop', $this->selectedContent)) {
            $contents = Posts::join('depts', 'posts.id_dept', '=', 'depts.id')
            ->leftJoin('sop_pdf', 'posts.id', '=', 'sop_pdf.id_post')
            ->leftJoin('pdf_pages', 'posts.id', '=', 'pdf_pages.id_post')
            ->where('sop_pdf.status', 'active')
            ->where(function ($query) use ($terms) {
                foreach ($terms as $term) {
                    $query->where('pdf_pages.content', 'LIKE', '%' . $term . '%')
                    ->orWhere('posts.title','LIKE', '%'. $term. '%');
                }
            })
            ->orderBy('posts.title', 'asc')
            ->orderBy('sop_pdf.id', 'asc')
            ->orderBy('pdf_pages.id', 'asc')
            // ->get();
            ->paginate($this->limitPerPage);
        }
        if(in_array('policy', $this->selectedContent)) {
            $contents2 = Posts::leftJoin('file_policy', 'posts.id', '=', 'file_policy.id_post')
            ->leftJoin('pdf_pages', 'posts.id', '=', 'pdf_pages.id_post')
            ->where('file_policy.status', 'active')
            ->where(function ($query) use ($terms) {
                foreach ($terms as $term) {
                    $query->where('pdf_pages.content', 'LIKE', '%' . $term . '%')
                    ->orWhere('posts.title','LIKE', '%'. $term. '%');
                }
            })
            ->orderBy('posts.title', 'asc')
            ->orderBy('file_policy.id', 'asc')
            ->orderBy('pdf_pages.id', 'asc')
            ->paginate($this->limitPerPage);
        }
        if(isset($contents) and isset($contents2)) {
            $merged_collection = $contents->merge($contents2);
        } else {
            if(isset($contents)) {
                $merged_collection = $contents;
            } else if(isset($contents2)) {
                $merged_collection = $contents2;
            } else {
                $merged_collection = [];
            }
        }
        $posts = array();
        foreach($merged_collection as $content)
        {
            array_push($posts, $content);
        }
        $this->contents = $posts;
    }
    public function render()
    {
        if(!empty($this->search)){
            $terms = array_filter(array_unique(explode(" ", $this->search)));
            $this->queryResults($terms);
        }else {
            $this->reset();
        }
        return view('livewire.components.modal-search');
    }
}
