<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;

class HighlightSearch extends Component
{
    public $text;
    public $id_post;
    public $title;
    public $dept;
    public $pageNum;
    public $searched;
    public function render()
    {
        $parse = array_filter(array_unique(explode(" ", $this->searched)));
        $arrSearch = array();
        foreach ($parse as $p) {
            if($p !== ""){
                $arrSearch[] = '/('.$p.')/i';
            }
        }
        return view('livewire.components.highlight-search',
        ['contents' =>
            preg_replace(
                $arrSearch,
                "<span class='highlighted'>$1</span>",
                preg_replace('/\s+/S', " ", $this->text))
                // /(\r\n)+|\r+|\n+|\t+/
        ]);
    }
}
