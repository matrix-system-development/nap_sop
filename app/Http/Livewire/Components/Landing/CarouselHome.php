<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class CarouselHome extends Component
{
    public function render()
    {
        $query = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->where('sp.status', 'active')
        ->orderBy('sp.created_at', 'desc')
        ->take(7)
        ->get();
        return view('livewire.landing.carousel-home', [
            'sops' => $query,
        ]);
    }
}
