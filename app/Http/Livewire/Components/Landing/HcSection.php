<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class HcSection extends Component
{
    public function render()
    {
        $query = Posts::join('hc_corners as hc', 'posts.id', '=', 'hc.id_post')
        ->orderBy('hc.created_at', 'desc')
        ->take(5)
        ->get();
        
        return view('livewire.landing.hc-section', [
            'corners' => $query,
        ]);
    }
}
