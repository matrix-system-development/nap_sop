<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class PolicyHome extends Component
{
    public function render()
    {
        $query = Posts::join('file_policy as fp', 'posts.id', '=', 'fp.id_post')
        ->orderBy('fp.created_at', 'desc')
        ->take(7)
        ->get();
        return view('livewire.landing.policy-home', [
            'policy' => $query,
        ]);
    }
}
