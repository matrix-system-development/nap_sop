<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class AnnouncementHome extends Component
{
    public $file_path, $announcement_id, $title;
    public $allData;
    public function render()
    {
        $announcement = Posts::join('announcements', 'posts.id', '=', 'announcements.id_post')
        ->orderBy('announcements.created_at', 'desc')
        ->take(3)
        ->get();
        return view('livewire.landing.announcement-home', [
            'announcement' => $announcement,
        ]);
        
        // return view('livewire.announcement-home');
    }
}
