<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class MatrixTips extends Component
{
    public function render()
    {
        $query = Posts::join('matrix_tips as mt', 'posts.id', '=', 'mt.id_post')
        ->orderBy('mt.created_at', 'desc')
        ->take(5)
        ->get();
        
        return view('livewire.landing.matrix-tips', [
            'tips' => $query,
        ]);
    }
}
