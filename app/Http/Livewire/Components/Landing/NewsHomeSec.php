<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class NewsHomeSec extends Component
{
    public function render()
    {
        $query = Posts::join('news as nw', 'posts.id', '=', 'nw.id_post')
        // ->orderBy('created_at', 'desc')
        ->take(3)
        ->get();
        return view('livewire.landing.news-home2', [
            'news2' => $query,
        ]);
    }
}
