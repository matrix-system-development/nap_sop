<?php

namespace App\Http\Livewire\Components\Marcomm;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\{TextProcessingTrait,LoggerTrait};
use App\Models\{Posts,MatrixTips,PostUserRel, Dept};

class CreateMatrixTips extends Component
{
    use WithFileUploads, TextProcessingTrait, LoggerTrait;

    public $title, $description, $thumbnail, $descriptionPreview, $thumbnailPreview;
    public $createdBy;
    public $preview = false;
    protected $listeners = [
        'refresh-contents' => 'getContent'
    ];
    protected $rules = [
        'title' => 'required|unique:posts,title|max:255',
        'description' => 'required',
        'thumbnail' => 'required|mimes:jpg,jpeg,png|max:2048',
    ];
    private function resetInput()
    {
        $this->title = null;
        $this->description = null;
        $this->thumbnail = null;
        $this->descriptionPreview = null;
        $this->thumbnailPreview = null;
        $this->preview = false;
    }

    public function getContent($term)
    {
        $this->description= $term['content'];
    }
    public function getPreview()
    {
        $this->preview = true;
        $this->descriptionPreview = $this->description;
        $this->thumbnailPreview = $this->thumbnail;
    }

    public function render()
    {
        return view('livewire.components.marcomm.create-matrix-tips');
    }

    public function submit()
    {
        $this->validate();
        $postModel = new Posts;
        $contentModel = new MatrixTips();
        $user_post = new PostUserRel;

        DB::transaction(function () use (
            $postModel,$contentModel,$user_post)
            {
                $postModel->title = $this->title;
                $postModel->id_dept = Dept::where('dept_code', 'mrc')->firstOrfail()->id;
                $postModel->seo_url = $this->string_seo($this->title);
                $postModel->save();
                $fileName = $postModel->id.'-'.$this->seo_friendly_url($this->limit_text($this->title, 6));
                $dom = new \DomDocument();
                $dom->loadHtml($this->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach($images as $k => $img){
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = '/public/uploads/matrix_tips/'.$fileName.'/contents'.'/'.
                    $fileName.'-'.time().$k.'.png';
                    $path = '/storage/uploads/matrix_tips/'.$fileName.'/contents'.'/'.
                    $fileName.'-'.time().$k.'.png';
                    Storage::put($image_name, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $path);
                }
                $this->description = $dom->saveHTML();
                $contentModel->contents = $this->description;
                $contentModel->id_post = $postModel->id;

                $this->thumbnail->storeAs('uploads/matrix_tips/'.$fileName.'/thumbnail', 
                $fileName.'.'.$this->thumbnail->getClientOriginalExtension(), 'public');
                $contentModel->thumbnail_path = '/storage/uploads/matrix_tips/'.$fileName.'/thumbnail/'.
                    $fileName.'.'.$this->thumbnail->getClientOriginalExtension();
                $contentModel->save();
                $user_post->id_post = $postModel->id;
                $user_post->id_user = auth()->user()->id;
                $user_post->name_user = auth()->user()->name;
                $user_post->save();
                $this->loggingActivity(['name'=>'CREATE_MATRIX_TIPS','subject_id'=> $contentModel, 'description' => 'Create Matrix Tips - '. $postModel->title]);
                $this->resetInput();
                return redirect()->route('tips.title', ['title' => $postModel->seo_url])->with('success', 'Create News Content Success - '.$postModel->title);
                // return redirect(request()->header('Referer'))->with('success', 'Content Has Been Created - '.$postModel->title);
            });
    }
}
