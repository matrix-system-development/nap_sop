<?php

namespace App\Http\Livewire\Components\Marcomm;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\{
    Posts, Dept, 
};

class PaginateTips extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sortParam, $sortDir;
    public function getQueryString()
    {
        return [];
    }

    public function render()
    {

        $tips = Posts::select('posts.title', 'posts.seo_url', 'posts.created_at', 'matrix_tips.thumbnail_path')
        ->join('matrix_tips', 'matrix_tips.id_post', '=','posts.id')
        ->orderBy('posts.'.(!is_null($this->sortParam) ? $this->sortParam : 'title'), 
            (!is_null($this->sortDir) ? $this->sortDir : 'ASC'))
        ->paginate(2);
        

        return view('livewire.components.marcomm.paginate-tips', compact('tips'));
    }
}
