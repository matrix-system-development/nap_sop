<?php

namespace App\Http\Livewire\Components\Hc;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\{
    Posts, Dept, 
};


class PaginateCorner extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sortParam, $sortDir;
    public function getQueryString()
    {
        return [];
    }

    public function render()
    {
        $hcorners = Posts::select('posts.title', 'posts.seo_url', 'posts.created_at', 'hc_corners.thumbnail_path')
        ->join('hc_corners', 'hc_corners.id_post', '=','posts.id')
        ->orderBy('posts.'.(!is_null($this->sortParam) ? $this->sortParam : 'title'), 
            (!is_null($this->sortDir) ? $this->sortDir : 'ASC'))
        ->paginate(2);
        return view('livewire.components.Hc.paginate-corner', compact('hcorners'));
    }
}
