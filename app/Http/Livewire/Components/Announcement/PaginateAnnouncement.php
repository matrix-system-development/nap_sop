<?php

namespace App\Http\Livewire\Components\Announcement;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\{
    Posts, Dept, 
};

class PaginateAnnouncement extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sortParam, $sortDir;
    public function getQueryString()
    {
        return [];
    }
    public function render()
    {   
        
        $announcement = Posts::select('posts.title', 'posts.seo_url', 'posts.created_at', 'announcements.thumbnail_path')
        ->join('announcements', 'announcements.id_post', '=','posts.id')
        ->orderBy('posts.'.(!is_null($this->sortParam) ? $this->sortParam : 'title'), 
            (!is_null($this->sortDir) ? $this->sortDir : 'ASC'))
        ->paginate(2);
        
        return view('livewire.components.announcement.paginate-announcement', compact('announcement'));
    }
}
