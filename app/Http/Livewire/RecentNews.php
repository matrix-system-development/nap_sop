<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\{Posts};

class RecentNews extends Component
{
    public function render()
    {
        $query = Posts::join('news as nw', 'posts.id', '=', 'nw.id_post')
        // ->join('post_user_rels', 'posts.id', '=', 'post_user_rels.id_user')
        ->orderBy('nw.created_at', 'desc')
        ->take(3)
        ->get();
        return view('livewire.recent-news', [
            'news' => $query,
        ]);
    }
}
