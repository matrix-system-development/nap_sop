<?php

namespace App\Http\Traits;
use Stevebauman\Location\Facades\Location;
use hisorange\BrowserDetect\Facade as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait LoggerTrait {
    public function loggingActivity($log)
    {
        $logger = activity(isset($log['name']) ? $log['name'] : 'default')
        ->causedBy(Auth::user())
        ->withProperties([
            'browser' => Browser::browserName(), 
            'ip' => request()->ip(),
        ]);
        if(isset($log['subject_id']))
        {
            $logger->performedOn($log['subject_id']);
        }
        $logger->log($log['description']);
    }
    public function countLogin()
    {
        
    }
}