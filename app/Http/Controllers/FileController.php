<?php

namespace App\Http\Controllers;
use App\Models\{File_sop,Posts,Dept, FilePolicy};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function view($id)
    {
        $files = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/"))]);
        return view('viewpdf',compact('files','path'));
    }
    public function viewIdPage($id, $page)
    {
        $files = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        // $page = $pageNum;
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/"))]);
        return view('viewpdf-page',compact('files','path', 'page'));
    }
    public function viewSOPAll()
    {
        $depts = Dept::whereIn('depts.dept_code', ['mrc', 'hcl', 'rnc'])
        ->orderby('dept_name', 'asc')->get();
        $others= Dept::whereNotIn('depts.dept_code', ['mrc', 'hcl', 'rnc'])
        ->orderby('dept_name', 'asc')->get();
        return view('Menu.Sop.newsdept', compact('depts', 'others'));
    }

    public function viewPolicyAll()
    {
        return view('Menu.Sop.policy');
    }

    public function viewSOPDept($id)
    {
        $sops = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        // ->join('post_user_rels', 'post_user_rels.id_post', '=', 'posts.id')
        ->where('depts.id',$id)
        ->orderBy('posts.created_at', 'desc')
        ->paginate(2);
        return view('Menu.Sop.newsdept', compact('sops'));
    }

    public function isolate($string, $additional = null)
    {
        if(isset($additional)) {
            if($additional == 'sop') {
                $query = File_sop::where('filename', $string)->firstOrFail();
            } else if($additional == 'policy') {
                $query = FilePolicy::where('filename', $string)->firstOrFail();
            }
        }
        $path = '/public/'.trim($query['file_path'],'/storage/');
        $getFiles = Storage::get($path);
        return $getFiles;
    }

    public function isolateThumbnail($string)
    {
        $query = File_sop::where('filename', $string)->firstOrFail();
        $thumbnail = trim($query['file_path'],$query['filename']);
        $path = '/public/'.trim($thumbnail,'/storage/').'/'.'thumbnail-'.$query['filename'];
        $getFiles = Storage::get($path);
        return $getFiles;
    }
    public function testView($string)
    {
        $files = new Request([
            'file_path'=> url('view/isolate/'.$string)
        ]);
        return view('viewpdf',compact('files'));
        // return view('home');
    }
}
