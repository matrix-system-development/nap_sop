<?php

namespace App\Http\Controllers;
use App\Models\Posts;


class BreadcrumbsController extends Controller
{
    public function getTitle($title)
    {
        try {
            $title = Posts::where('seo_url', $title)->firstOrFail();
            return $title['title'];
        } catch (\Exception $e) {
            return ' ';
        }
    }
}