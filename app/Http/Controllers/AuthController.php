<?php

namespace App\Http\Controllers;

use App\Models\{User, RuleLogin};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function login($q = null)
    {
        if (Auth::check()) {
            return redirect('home');
        }
        else{
            $checkRule = RuleLogin::where('rule_name','sso')->first();
            if($checkRule)
            {
                if(($checkRule->rule_value == '1'))
                {
                    if($q !== null)
                    {
                        return $this->doLoginSSO($q);
                    }
                    else
                    {
                        return redirect(RuleLogin::where('rule_name','sso logout')->first()->rule_value);
                    }
                }
                else if(RuleLogin::where('rule_name','sso')->first()->rule_value == '0')
                {
                    return view('Auth.login');
                }
            }
            else{
                return view('Auth.login');
            }
        }
    }

    public function doLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $check = User::where('email', $credentials['email'])->first();
        if(Auth::attempt($credentials)){
            if($check->is_active == true){
                $request->session()->regenerate();
                User::find($check->id)->increment('login_count');
                $this->loggingActivity(['name'=> 'LOGIN_SUCCESS','subject_id'=> $check, 'description' => 'Login']);
                return redirect('/home');
            }
            else{
                return redirect('/')->with('statusLogin','Give Access First to User');
            }
        }
        else{
            return redirect('/')->with('statusLogin','Wrong Email or Password');
        }
    }

    public function doLoginSSO($q)
    {
        $email = base64_decode($q);
        $password = 'password';

        $dummy = new Request([
            'email' => $email,
            'password' => $password
        ]);

        $credentials = $dummy->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $cekuser_status=User::where('email',$email)->first();
        $dologin=Auth::attempt($credentials);
        if($dologin){
            if($cekuser_status->is_active==true)
            {
                User::find($cekuser_status->id)->increment('login_count');
                $this->loggingActivity(['name'=> 'LOGIN_SUCCESS','subject_id'=> $cekuser_status, 'description' => 'Login']);
                return redirect()->intended('home')
                ->withSuccess('Signed in');
            }
        }
        else{
            return redirect('https://intranet.nap.net.id/sso'); //kalau pakai sso
        }
    }
    public function doLogout(Request $request)
    {
        $this->loggingActivity(['name'=> 'LOGOUT_SUCCESS', 'description' => 'Logout']);
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}
