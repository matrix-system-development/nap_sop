<?php

namespace App\Http\Controllers;
use App\Models\{File_sop, Posts, News};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use App\Http\Traits\TextProcessingTrait;

class ViewController extends Controller
{
    use TextProcessingTrait;

    public function viewNews($title)
    {
        $news = News::join('posts', 'posts.id', '=','news.id_post')
        ->where('posts.seo_url', $title)
        ->join('post_user_rels', 'posts.id', '=', 'post_user_rels.id_post')
        ->firstOrFail();
        $news['contents'] = $this->sourceContentImageFixer($news['contents']);
        $by_user = strtolower($this->seo_friendly_url($news->name_user));
        $this->loggingActivity(['name'=> 'VIEW_NEWS','subject_id'=> $news, 'description' => 'View News - '. $news->title]);
        return view('view-news',compact('news','by_user'));
    }
    public function viewNewsAll()
    {
        return view('Menu.News.allnews');
    }
    public function viewAnnouncementAll()
    {
        return view('Menu.Announcement.announcement');
    }
    public function viewHCornerAll()
    {
        return view('Menu.HC.hcorner');
    }
    public function viewTipsAll()
    {
        return view('Menu.Marcomm.alltips');
    }

    public function newsByUser($string)
    {
        $query = Posts::join('post_user_rels', 'posts.id', 'post_user_rels.id_post')
        ->join('news', 'news.id', '=', 'post_user_rels.id_user')
        ->where('post_user_rels.name_user', 'like', '%'.$string.'%')
        ->paginate(10);
        $by = $string;
        // return $query;
        return view("Menu.News.news-user", compact('by'));
    }

    public function viewSOP($title)
    {
        try {
            $files = Posts::join('depts', 'depts.id', '=','posts.id_dept')
            ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
            ->where('posts.seo_url',$title)
            ->where('sop_pdf.status', 'active')
            ->firstOrFail();
            $additional = 'sop';
            $page = 1;
            $this->loggingActivity([
                'name'=> 'VIEW_SOP',
                'subject_id'=> File_sop::where('id_post', $files->id_post)->firstOrFail(),
                'description' => 'View SOP - '. $files->title]
            );
            $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/").'/sop')]);
            return view('viewpdf',compact('files','path', 'page', 'additional'));
            // return view('viewpdf',compact('files','path', 'additional'));
        } catch (\Exception $e) {
            return redirect()->route('sop.all')->with('message', 'file does not exist!');
        }
    }

    public function viewPolicy($title)
    {
        $files = Posts::join('file_policy', 'file_policy.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $additional = 'policy';
        $page = 1;
        // return $files;
        // $this->loggingActivity([
        //     'name'=> 'VIEW_POLICY',
        //     'subject_id'=> File_sop::where('id_post', $files->id_post)->firstOrFail(),
        //     'description' => 'View Policy - '. $files->title]
        // );
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/").'/policy')]);
        return view('viewpdf',compact('files','path', 'page', 'additional'));
    }
    public function viewIdPage($title, $page)
    {
        $files = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('sop_pdf.status', 'active')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $additional = 'sop';
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/").'/sop')]);
        // return view('viewpdf',compact('files','path', 'additional'));
        return view('viewpdf',compact('files','path', 'page', 'additional'));
    }

    public function viewIdPagePolicy($title, $page)
    {
        $files = Posts::join('file_policy', 'file_policy.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $additional = 'policy';
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/").'/policy')]);
        return view('viewpdf',compact('files','path', 'page', 'additional'));
    }


    public function viewAnnouncement($title)
    {
        $announcement = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('announcements', 'announcements.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $announcement['contents'] = $this->sourceContentImageFixer($announcement['contents']);
        $this->loggingActivity(['name'=>'VIEW_ANNOUNCEMENT','subject_id'=> $announcement, 'description' => 'View Announcement - '. $announcement->title]);
        return view('view-announcement',compact('announcement'));
    }

    public function viewHCorner($title)
    {
        $announcement = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('hc_corners', 'hc_corners.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $announcement['contents'] = $this->sourceContentImageFixer($announcement['contents']);
        $this->loggingActivity(['name'=>'VIEW_HC_CORNER','subject_id'=> $announcement, 'description' => 'View HC Corner - '. $announcement->title]);
        return view('view-announcement',compact('announcement'));
    }
    public function viewTips($title)
    {
        $announcement = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('matrix_tips', 'matrix_tips.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $announcement['contents'] = $this->sourceContentImageFixer($announcement['contents']);
        $this->loggingActivity(['name'=>'VIEW_MATRIX_TIPS','subject_id'=> $announcement, 'description' => 'View Matrix Tips - '. $announcement->title]);
        return view('view-announcement',compact('announcement'));
    }
}
