<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Models\{ActivityLog, User,Posts,File_sop,Dept, PDFDesc, Announcement,DeptPrivilage};
use DataTables;
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
// use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function getPosts(){
        $data = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('sp.status', 'active');
        if(request('filter_dept')) {
            $data->where('depts.dept_name',request('filter_dept'));
        }
        if(request('filter_search')) {
            $data
            // ->where('depts.dept_name', 'like', '%' . request('filter_search') . '%')
            ->where('posts.title', 'like', '%' . request('filter_search') . '%');
        }
        $data = $data->get(['posts.id', 'posts.title','depts.dept_name', 'sp.status', 'sp.created_at','sp.id_post']);

        // return $data;
        $data2 = array();
        if(!empty($data))
        {
            foreach ($data as $d)
            {
                $show =  route('view.id',$d->id_post);
                $nestedData['title'] = $d->title;
                $nestedData['dept_name'] = $d->dept_name;
                $nestedData['status'] = strtoupper($d->status);
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($d->created_at));
                $nestedData['action'] = "
                <div class='row'>
                <div class='col-12'><a href='{$show}' title='SHOW' class=' btn btn-primary btn-sm' target='_blank'><i class='fas fa-book-open'></i>&emsp;View</a> </div>
                </div>
                ";
                $data2[] = $nestedData;
            }
        }
        return Datatables::of($data2)->make(true);
    }

    public function index()
    {
        $dept =  Dept::get();
        return view('Menu.Landing.home-livewire', compact('dept'));
    }
    public function view()
    {
        $dept =  Dept::get();
        return view('sop-main', compact('dept'));
    }
    public function dummy()
    {
        return redirect()->route('admin.sop.form.sop');
        // dd(base64_encode('enquity.ekayekti@napinfo.co.id'));
        // return $dept_priv;
    }
}
