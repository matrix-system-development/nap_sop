<?php

namespace App\Http\Controllers\withAuth;
use App\Models\{Posts, HcCorner};
use App\Http\Traits\TextProcessingTrait;

class MatrixTipsController extends Controller
{
    use TextProcessingTrait;
    public function __construct()
    {
        $this->middleware(["role:admin", "dept:mrc"]);
    }
    public function view($id)
    {
        
    }
    public function createTips() 
    {
         return view("Admin.Tips.createtips");
    }
}
