<?php

namespace App\Http\Controllers\withAuth;
use App\Models\{Posts, HcCorner};
use App\Http\Traits\TextProcessingTrait;

class HcController extends Controller
{
    use TextProcessingTrait;
    public function __construct()
    {
        $this->middleware(["role:admin", "dept:hcl"]);
    }
    public function view($id)
    {
        
    }
    public function createCorner() 
    {
         return view("Admin.Hc.createcorner");
    }
}
