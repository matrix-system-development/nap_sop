<?php

namespace App\Http\Controllers\withAuth;

use Illuminate\Http\Request;
use App\Models\{
    Posts, Dept,
    FilePolicy,
    HistorySOP, History_desc,
    HistoryPolicy,
    PDFDesc, PDFPage, PolicyPage, PostUserRel
};
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use App\Http\Traits\{TextProcessingTrait,LoggerTrait};


class PolicyController extends Controller
{
    use TextProcessingTrait,LoggerTrait;
    public function __construct()
    {
        $this->middleware(["role:admin"]);
    }

    public function uploadView()
    {
        return view("Admin.Policy.upload");
    }

    public function updateView()
    {
        $remove = new \Illuminate\Filesystem\Filesystem;
        $remove->cleanDirectory(public_path('storage/uploads/livewire-tmp'));
        $dept = Dept::get();
        return view("Admin.Update.update", compact('dept'));
    }
    public function updateViewId($id)
    {
            $posts = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
            ->join('depts', 'posts.id_dept', '=', 'depts.id')
            ->where('sop_pdf.status', 'active')
            ->where('posts.id',$id)
            ->firstOrFail();
            $path = new Request(['file_path'=> url('view/isolate/'.trim($posts->file_path,"/storage/uploads/"))]);
            return view("Admin.Update.updateViewId",compact('posts', $path));
    }
    public function fileUpload(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
        ]);
        $path = public_path()."/storage/uploads/test/".Auth::id().'/'.$request->urlPdf;
        $fileModel = new FilePolicy;
        $postModel = new Posts;
        $history = new HistoryPolicy;
        $history_desc = new History_desc;
        $pdf_descs = new PDFDesc;
        $user_post = new PostUserRel;
        $parser = new \Smalot\PdfParser\Parser();
        $count_page = 1;
        DB::transaction(function () use (
            $postModel, $fileModel, $history,
            $history_desc,$pdf_descs,
            $parser,$count_page,$user_post, $request,$path)
            {
                $postModel->title = $request->title;
                $postModel->seo_url = $this->seo_friendly_url($this->limit_text($request->title, 6)).'-'.time();
                if(in_array(mime_content_type($path), array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document')))
                {
                    // $fileModel->filename = time().'_'.
                    // preg_replace('/\s+/', '_', $this->title).'.pdf';
                    // dd($this->converted);
                    // $fileModel->file_path = '/storage/uploads/'.$fileModel->filename;
                }
                else
                {
                    $fileModel->filename =  $postModel->seo_url.'-'.$request->urlPdf;
                    Storage::makeDirectory('public/uploads/policy/'.$postModel->seo_url);
                    File::copy(storage_path('app/public/uploads/test/'.Auth::id().'/'.$request->urlPdf), storage_path('app/public/uploads/policy/'.$postModel->seo_url.'/'.$fileModel->filename));
                    $fileModel->file_path  = '/storage/uploads/policy/'.$postModel->seo_url.'/'.$fileModel->filename;
                }
                $postModel->save();
                $fileModel->status = 'active';
                $fileModel->id_post = $postModel->id;
                $fileModel->save();
                $history->id_post = $postModel->id;
                $history->save();
                $history_desc->id_history_policy = $history->id;
                $history_desc->desc = 'first upload';
                $history_desc->save();

                $parserPdf = $parser->parseFile(storage_path('app/public/uploads/policy/'.$postModel->seo_url.'/'.$fileModel->filename));
                $pdf_descs->id_policy = $fileModel->id;
                $pdf_descs->author = isset($parserPdf->getDetails()['Author']) ? $parserPdf->getDetails()['Author'] : null;
                $pdf_descs->creator = isset($parserPdf->getDetails()['Creator']) ? $parserPdf->getDetails()['Creator'] : null;
                $pdf_descs->pages = $parserPdf->getDetails()['Pages'];
                $pdf_descs->save();

                $data_page = [];
                try {
                    $forLoop = $parserPdf->getPages();
                    foreach ($forLoop as $page) {
                        $pdf_page = new PolicyPage;
                        $pdf_page->id_post = $postModel->id;
                        $pdf_page->page = $count_page;
                        $pdf_page->content =  preg_replace('/\s+/', ' ',$page->getText());
                        $data_page[] = $pdf_page->attributesToArray();
                        $count_page++;
                    }

                } catch (\Exception $e) {
                    $pdf_page = new PDFPage;
                    $pdf_page->id_policy = $postModel->id;
                    $pdf_page->page = $count_page;
                    $data_page[] = [
                        "id_post"=> $postModel->id,
                        "page"=> $pdf_page->page ,
                        "content"=> " "
                    ];
                }
                $user_post->id_post = $postModel->id;
                $user_post->id_user = auth()->user()->id;
                $user_post->name_user = auth()->user()->name;
                PDFPage::insert($data_page);
            $this->loggingActivity(['name'=>'UPLOAD_POLICY','subject_id'=> $fileModel, 'description' => 'Upload Company Policy - '. $postModel->title]);
          $successStatus = true;
          Storage::deleteDirectory('/public/uploads/test/'.Auth::id());
        });

        if(isset($successStatus))
        {
            return redirect()->route('policy.title', ['title' => $postModel->seo_url])->with('success', 'Berhasil Upload File Baru  - '.$postModel->title);
        }
        else
        {
            return redirect()->back()->withErrors(['msg' => 'Error Uploading File']);
        }
    }

    public function uploadSplitPolicy(Request $request)
    {
        $receiver = new FileReceiver('file', $request, HandlerFactory::classFromRequest($request));
        if (!$receiver->isUploaded()) {
            throw new UploadMissingFileException();
        }
        $fileReceived = $receiver->receive(); // receive file
        if ($fileReceived->isFinished()) { // file uploading is complete / all chunks are uploaded
            $file = $fileReceived->getFile(); // get file
            $extension = $file->getClientOriginalExtension();
            $fileName = str_replace('.'.$extension, '',md5(time())); //file name without extenstion
            $fileName .= '_' .time(). '.' . $extension; // a unique file name
            $disk = Storage::disk(config('filesystems.default'));
            $path = $disk->putFileAs('public/uploads/test/'.Auth::id(), $file, $fileName);
            $path_render = url('storage/uploads/test/'.Auth::id().'/'.$fileName);
            unlink($file->getPathname());
            $thumbnail_render = url('storage/uploads/test/'.Auth::id().'/'.$fileName);
            return [
                'path' => $path_render,
                'pdf_name' => $fileName,
                'filename' => $fileName
            ];
        }
        $handler = $fileReceived->handler();
        return [
            'done' => $handler->getPercentageDone(),
            'status' => true
        ];


    }


}
