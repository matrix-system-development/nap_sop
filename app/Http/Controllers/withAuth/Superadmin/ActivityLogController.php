<?php

namespace App\Http\Controllers\withAuth\Superadmin;
use App\Models\{ActivityLog};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{    
    public function view() 
    {
        $activity_log = ActivityLog::with('user')->limit(10)->orderBy('id', 'desc')->paginate(10);

        $this->loggingActivity(['name'=> 'VIEW_ACTIVITY_LOG', 'description' => 'Activity Log']);
        // $this->loggingActivity('Activiy Log');
        return view("Superadmin.view-activity", compact('activity_log'));
    }
}
