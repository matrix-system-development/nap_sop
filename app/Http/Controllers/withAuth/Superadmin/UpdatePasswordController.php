<?php

namespace App\Http\Controllers\withAuth\Superadmin;
use App\Models\{Posts, News};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class UpdatePasswordController extends Controller
{    
    public function update() 
    {
         return view("Superadmin.update-password");
    }

}
