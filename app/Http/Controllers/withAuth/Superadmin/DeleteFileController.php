<?php

namespace App\Http\Controllers\withAuth\Superadmin;
use App\Models\{Posts, News};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class DeleteFileController extends Controller
{    
    public function ViewDeleteUnusedFile() 
    {
         return view("Superadmin.delete-unused");
    }

    public function ProcessDeleteUnusedFile() 
    {
        try {
            $folderArray = ['announcements', 'news', 'matrix_tips', 'hc_corners'];
            $tempArray = [];
            foreach ($folderArray as $primary) {
                $files = Storage::allFiles('/public/uploads/'.$primary);
                foreach ($files as $dir) {
                    $find = str_replace('public/uploads/'.$primary.'/', "", $dir);
                    $trim = substr($find, 0, strpos($find, "/thumbnail/"));
                    if($trim != '') 
                    {
                        $tempArray[] = $trim;
                    }
                }
                foreach ($tempArray as $dir2) {
                    $query = Posts::join($primary, 'posts.id', '=', $primary.'.id_post')
                    ->where('thumbnail_path', 'LIKE', '%'.$dir2.'%')
                    ->first();
                    if(!$query)
                    {
                        Storage::deleteDirectory('/public/uploads/'.$primary.'/'.$dir2);
                    }
                }
            }
            $deleteSOP = Storage::allFiles('/public/uploads/sop');
            $sopArray = [];
            foreach ($deleteSOP as $del) {
                $find = str_replace('public/', "storage/", $del);
                $syncSOP =  Posts::join('sop_pdf', 'posts.id', '=','sop_pdf.id_post')
                ->where('sop_pdf.file_path', 'LIKE', '%'.$find.'%')
                ->first();
                if(!$syncSOP)
                {
                    $trimSOPPath = preg_replace('/^(storage\/uploads\/sop\/(.*)\/)/m', "", $find);
                    $sopArray[] = $trimSOPPath;
                    $finalPath = str_replace($trimSOPPath, '', $del);
                    Storage::deleteDirectory('/'.$finalPath);
                } 
            }
            return redirect()->back()->with('success', 'Success Deleting File!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Failed');
        }
    }

}