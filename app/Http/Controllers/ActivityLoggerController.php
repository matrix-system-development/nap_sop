<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Stevebauman\Location\Facades\Location;
use Illuminate\Http\Request;

class ActivityLoggerController extends Controller
{
    public function logging(Request $request)
    {
        activity()->withProperties(
            ['browser' => $request->server('HTTP_USER_AGENT'), 
            'ip' => $request->ip(), 
            'coordinates' => $request])
            ->log('Home Page');
            return $request;
    }
}
