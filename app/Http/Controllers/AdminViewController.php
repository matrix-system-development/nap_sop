<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{
    Posts, File_sop, Dept,
    HistorySOP, History_desc,
    PDFDesc, PDFPage,
};

use DataTables;
class AdminViewController extends Controller
{
    public function __construct()
    {
        $this->middleware(["role:admin"]);
    }
    public function upload()
    {
        $dept = Dept::get();
        return view("Admin.Upload.upload", compact('dept'));
    }
   public function update() 
   {
        $remove = new \Illuminate\Filesystem\Filesystem;
        $remove->cleanDirectory(public_path('storage/uploads/livewire-tmp'));
        $dept = Dept::get();
        return view("Admin.Update.update", compact('dept'));
   }
   public function updateViewId($id) 
   {
        $posts = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        $path = new Request(['file_path'=> url('view/isolate/'.trim($posts->file_path,"/storage/uploads/"))]);
        return view("Admin.Update.updateViewId",compact('posts', $path));
   }
    public function getSOPDatatablesAdmin()
    {
        $data = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('sp.status', 'active');
        if(request('filter_dept')) {
            $data->where('depts.dept_name',request('filter_dept'));
        }
        if(request('filter_search')) {
            $data
            // ->where('depts.dept_name', 'like', '%' . request('filter_search') . '%')
            ->where('posts.title', 'like', '%' . request('filter_search') . '%');
        }
        $data = $data->get(['posts.id', 'posts.title','depts.dept_name', 'sp.status', 'sp.created_at','sp.id_post']);

        $data2 = array();
        if(!empty($data))
        {
            foreach ($data as $d)
            {
                $show =  route('view.id',$d->id_post);
                $update =  route('updateViewId',$d->id_post);
                $nestedData['title'] = $d->title;
                $nestedData['dept_name'] = $d->dept_name;
                $nestedData['status'] = strtoupper($d->status);
                $nestedData['created_at'] = date('j M Y',strtotime($d->created_at));
                $nestedData['action'] = "
                <div class='btn-group'>
                <button type='button' class='btn btn-info'>Action</button>
                <button type='button' class='btn btn-info dropdown-toggle dropdown-hover dropdown-icon' data-toggle='dropdown' aria-expanded='false'>
                    <span class='sr-only'>Toggle Dropdown</span>
                </button>
                <div class='dropdown-menu' role='menu' style=''>
                    <a class='dropdown-item' href='{$show}' target='_blank'>View</a>
                    <a class='dropdown-item' href='{$update}' target='_blank'>Update</a>
                </div>
                </div>
                ";
                $data2[] = $nestedData;
            }
        }
        return Datatables::of($data2)->make(true);
    }
    public function createNews() 
    {
         return view("Admin.News.createnews");
    }
}
