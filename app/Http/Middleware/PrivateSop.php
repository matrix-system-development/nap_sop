<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\File_sop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrivateSop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->hasAnyDept('rnc')) {
            return $next($request);
        }
        if($request->user()->hasRole('superadmin'))
        {
            return $next($request);
        }
        $checkPrivate = (new File_sop)->isPrivate(last($request->segments()));
        if($checkPrivate[0] == true) {
            if(((new File_sop)->isUserCanSee($checkPrivate))) {
                return $next($request);
            } else {
                abort(401, 'This action is unauthorized.');
            }
        }
        return $next($request);
    }
}
