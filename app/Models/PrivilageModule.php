<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrivilageModule extends Model
{
    use HasFactory;
    protected $fillable = [
        'privilage_code',
        'privilage_name',
    ];
}
