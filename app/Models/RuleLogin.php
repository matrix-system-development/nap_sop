<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RuleLogin extends Model
{
    use HasFactory;
    protected $table = 'rule_logins';
    protected $fillable=[
        'rule_name',
        'rule_value'
    ];
}
