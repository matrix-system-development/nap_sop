<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Spatie\ActivityLog\Traits\LogsActivity;

class ActivityLog extends Model
{
    use HasFactory;
    // use LogsActivity;

    protected $table = 'activity_log';

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'causer_id');
    }
    public function post()
    {
        return $this->belongsTo(\App\Models\Posts::class, 'subject_id');
    }
    public function news()
    {
        return $this->belongsTo(\App\Models\News::class, 'subject_id');
    }
    public function sop()
    {
        return $this->belongsTo(\App\Models\File_sop::class, 'subject_id');
    }
    public function announcement()
    {
        return $this->belongsTo(\App\Models\Announcement::class, 'subject_id');
    }

}
