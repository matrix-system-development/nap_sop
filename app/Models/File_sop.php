<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File_sop extends Model
{
    use HasFactory;
    protected $table = 'sop_pdf';
    protected $fillable = [
        'filename',
        'file_path',
        'status',
        'id_post',
        'is_update',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function sops()
    {
        return $this
            ->belongsToMany(File_sop::class)
            ->withTimestamps();
    }

    public function posts()
    {
        return $this
            ->belongsTo(Posts::class, 'id_post');
    }

    public function private()
    {
        return $this
            ->hasMany(SopPrivate::class, 'id','id_sop');
            // ->withTimestamps();
    }

    public function isPrivate($private)
    {
        $isPrivate = SopPrivate::where('id_sop', File_sop::where('id_post',Posts::where('seo_url', $private)->first()->id)->first()->id)
        ->first();
        if($isPrivate) {
            return [true, $isPrivate->id_sop];
        }
        return false;
    }

    public function isUserCanSee($private)
    {
        if($private[0] == true) {
            $checkUserDept = DB::table('dept_user')->where('user_id', Auth::user()->id)->first()->dept_id;
            $checkIsPrivate = SopPrivate::where('id_dept', $checkUserDept)->where('id_sop', $private[1])->first();
            if($checkIsPrivate) {
                return true;
            }
        }
        return false;
    }
}
