<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;
    use Search;
    protected $table = 'posts';
    protected $fillable = [
        'title',
        'seo_title',
        'id_dept',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];

}
