<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryPolicy extends Model
{
    use HasFactory;
    protected $table = 'history_policy'; 
    protected $fillable = [
        'id_post',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];


}
