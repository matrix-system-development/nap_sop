<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PDFPage extends Model
{
    use HasFactory;
    protected $table = 'pdf_pages';
    protected $fillable = [
        'id_post',
        'content',
        'page',
    ];
}
