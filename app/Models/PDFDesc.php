<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PDFDesc extends Model
{
    use HasFactory;
    protected $table = 'pdf_desc';
    protected $fillable = [
        'id_post',
        'author',
        'creator',
        'pages',
    ];
}
