<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PolicyPage extends Model
{
    use HasFactory;
    protected $table = 'policy_pages';
    protected $fillable = [
        'id_policy',
        'content',
        'page',
    ];
}
