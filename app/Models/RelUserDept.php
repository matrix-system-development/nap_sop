<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelUserDept extends Model
{
    use HasFactory;
    protected $table = 'dept_user'; 
    protected $fillable = [
        'dept_id',
        'user_id',
    ];

}
