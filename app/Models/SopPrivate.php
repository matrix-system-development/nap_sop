<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SopPrivate extends Model
{
    use HasFactory;
    protected $table = 'sop_private';
    protected $fillable = [
        'id_sop',
        'id_dept'
    ];
}
