<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeptPrivilage extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_dept',
        'id_privilage',
    ];
}
