<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostUserRel extends Model
{
    use HasFactory;
    protected $table = 'post_user_rels'; 
    protected $fillable = [
        'id_post',
        'id_user',
        'name_user',
    ];
}
