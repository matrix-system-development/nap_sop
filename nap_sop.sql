-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2022 at 12:23 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nap_sop`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'LOGIN_SUCCESS', 'Login', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"browser\":\"Chrome 97.0.4692\",\"ip\":\"127.0.0.1\"}', '2022-01-25 04:53:55', '2022-01-25 04:53:55'),
(2, 'LOGIN_SUCCESS', 'Login', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 09:22:26', '2022-02-23 09:22:26'),
(3, 'LOGOUT_SUCCESS', 'Logout', NULL, NULL, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 09:24:23', '2022-02-23 09:24:23'),
(4, 'LOGIN_SUCCESS', 'Login', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 09:33:12', '2022-02-23 09:33:12'),
(5, 'CREATE_USER', 'Create New User - frian.maulana@napinfo.co.id', 'App\\Models\\User', 3, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 09:33:37', '2022-02-23 09:33:37'),
(6, 'LOGOUT_SUCCESS', 'Logout', NULL, NULL, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 09:33:54', '2022-02-23 09:33:54'),
(7, 'LOGIN_SUCCESS', 'Login', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 10:02:31', '2022-02-23 10:02:31'),
(8, 'LOGOUT_SUCCESS', 'Logout', NULL, NULL, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 10:54:00', '2022-02-23 10:54:00'),
(9, 'LOGIN_SUCCESS', 'Login', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"browser\":\"Firefox 97\",\"ip\":\"127.0.0.1\"}', '2022-02-23 10:54:05', '2022-02-23 10:54:05'),
(10, 'LOGIN_SUCCESS', 'Login', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"browser\":\"Chrome 98.0.4758\",\"ip\":\"127.0.0.1\"}', '2022-02-23 11:20:28', '2022-02-23 11:20:28');

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `contents` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `id_post` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `depts`
--

CREATE TABLE `depts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dept_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `depts`
--

INSERT INTO `depts` (`id`, `dept_code`, `dept_name`, `created_at`, `updated_at`) VALUES
(1, 'rnc', 'Risk and Compliance', NULL, NULL),
(2, 'mrc', 'Marketing Communication', NULL, NULL),
(3, 'syd', 'System Development', NULL, NULL),
(4, 'hcl', 'Human Capital', NULL, NULL),
(5, 'cse', 'Customer Care', NULL, NULL),
(6, 'sas', 'Sales Support', NULL, NULL),
(7, 'ens', 'Enterprise Sales', NULL, NULL),
(8, 'whs', 'Wholesales Sales', NULL, NULL),
(9, 'kas', 'Key Account Sales', NULL, NULL),
(10, 'brs', 'Broadband Sales', NULL, NULL),
(11, 'gvs', 'Government Sales', NULL, NULL),
(12, 'ant', 'Accounting & Tax', NULL, NULL),
(13, 'fin', 'Finance', NULL, NULL),
(14, 'bcl', 'Budget Control', NULL, NULL),
(15, 'rae', 'Revenue Assurance', NULL, NULL),
(16, 'pnp', 'Property & Partnership', NULL, NULL),
(17, 'prc', 'Procurement', NULL, NULL),
(18, 'fam', 'Facility Asset & Management', NULL, NULL),
(19, 'sya', 'IT & System Administrator', NULL, NULL),
(20, 'ned', 'Network Development', NULL, NULL),
(21, 'neo', 'Network Operations', NULL, NULL),
(22, 'ney', 'Network Delivery', NULL, NULL),
(23, 'nei', 'Network Quality Improvement', NULL, NULL),
(24, 'dac', 'Data Center', NULL, NULL),
(25, 'cls', 'Cable Landing Station', NULL, NULL),
(26, 'lgl', 'Legal', NULL, NULL),
(27, 'crp', 'Corporate Planning', NULL, NULL),
(28, 'ina', 'Internal Audit', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dept_privilages`
--

CREATE TABLE `dept_privilages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_dept` bigint(20) UNSIGNED NOT NULL,
  `id_privilage` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dept_privilages`
--

INSERT INTO `dept_privilages` (`id`, `id_dept`, `id_privilage`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 4, 2, NULL, NULL),
(4, 2, 3, NULL, NULL),
(5, 4, 4, NULL, NULL),
(6, 2, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dept_user`
--

CREATE TABLE `dept_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dept_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dept_user`
--

INSERT INTO `dept_user` (`id`, `dept_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, NULL, NULL),
(2, 3, 2, NULL, NULL),
(3, 1, 3, '2022-02-23 09:33:37', '2022-02-23 09:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_corners`
--

CREATE TABLE `hc_corners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `contents` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `id_post` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_descs`
--

CREATE TABLE `history_descs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_history` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_sop`
--

CREATE TABLE `history_sop` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_post` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `matrix_tips`
--

CREATE TABLE `matrix_tips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `contents` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `id_post` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_09_06_103218_create_depts_table', 1),
(6, '2021_09_06_103311_create_posts_table', 1),
(7, '2021_09_06_120357_create_file_sops_table', 1),
(8, '2021_09_07_014550_create_history_s_o_p_s_table', 1),
(9, '2021_09_09_172153_create_history_descs_table', 1),
(10, '2021_09_14_151745_create_pdf_descs_table', 1),
(11, '2021_09_14_154056_create_pdf_pages_table', 1),
(12, '2021_10_11_120130_create_news_table', 1),
(13, '2021_10_14_144901_create_roles_table', 1),
(14, '2021_10_14_145252_create_role_user_table', 1),
(15, '2021_10_14_173315_create_dept_user_table', 1),
(16, '2021_10_18_011242_create_post_user_rels_table', 1),
(17, '2021_10_29_173616_create_announcements_table', 1),
(18, '2021_11_09_014333_create_hc_corners_table', 1),
(19, '2021_11_09_035824_create_matrix_tips_table', 1),
(20, '2021_11_09_051919_create_activity_log_table', 1),
(21, '2021_12_03_163147_create_rule_logins_table', 1),
(22, '2021_12_06_120041_create_privilage_modules_table', 1),
(23, '2021_12_06_121029_create_dept_privilages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `contents` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `id_post` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pdf_desc`
--

CREATE TABLE `pdf_desc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pages` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_pdf` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pdf_pages`
--

CREATE TABLE `pdf_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_pdf` bigint(20) UNSIGNED NOT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_dept` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_user_rels`
--

CREATE TABLE `post_user_rels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_post` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `name_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privilage_modules`
--

CREATE TABLE `privilage_modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `privilage_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privilage_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `privilage_modules`
--

INSERT INTO `privilage_modules` (`id`, `privilage_code`, `privilage_name`, `created_at`, `updated_at`) VALUES
(1, 'sop', 'SOP', NULL, NULL),
(2, 'ann', 'Announcement', NULL, NULL),
(3, 'nws', 'News', NULL, NULL),
(4, 'hcr', 'HC Corner', NULL, NULL),
(5, 'mtp', 'Matrix Tips', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin', NULL, NULL),
(2, 'admin', 'Admin', NULL, NULL),
(3, 'user', 'User', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 3, 3, '2022-02-23 09:33:37', '2022-02-23 09:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `rule_logins`
--

CREATE TABLE `rule_logins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rule_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rule_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rule_logins`
--

INSERT INTO `rule_logins` (`id`, `rule_name`, `rule_value`, `created_at`, `updated_at`) VALUES
(1, 'sso', '0', NULL, NULL),
(2, 'sso logout', 'https://intranet.nap.net.id/sso/', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sop_pdf`
--

CREATE TABLE `sop_pdf` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `id_post` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `login_count` int(11) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `is_active`, `login_count`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Riandi Kartiko', 'riandi.kartiko@napinfo.co.id', 1, 6, NULL, '$2y$10$At2HmeJkewFJnyMjWONEyOH3qc0WR/uh50ha8qbws./WOfXp/l5Ju', NULL, NULL, '2022-02-23 11:20:28'),
(2, 'Enquity Rizky Ekayekti', 'enquity.ekayekti@napinfo.co.id', 1, 0, NULL, '$2y$10$pNRL.V/bWsUYg.LsfaE9qu4uN2RthyWVTLY6NUitGU3idX0/XW3wS', NULL, NULL, NULL),
(3, 'Frian Maulana', 'frian.maulana@napinfo.co.id', 1, 0, NULL, '$2y$10$ACaBInrYSPZ6yLOMn0Lm8uZ4brpebARE0a8wr9UPHPEuT4XYQk.jG', NULL, '2022-02-23 09:33:37', '2022-02-23 09:33:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject` (`subject_type`,`subject_id`),
  ADD KEY `causer` (`causer_type`,`causer_id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announcements_id_post_foreign` (`id_post`);

--
-- Indexes for table `depts`
--
ALTER TABLE `depts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dept_privilages`
--
ALTER TABLE `dept_privilages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_privilages_id_dept_foreign` (`id_dept`),
  ADD KEY `dept_privilages_id_privilage_foreign` (`id_privilage`);

--
-- Indexes for table `dept_user`
--
ALTER TABLE `dept_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_user_dept_id_foreign` (`dept_id`),
  ADD KEY `dept_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `hc_corners`
--
ALTER TABLE `hc_corners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hc_corners_id_post_foreign` (`id_post`);

--
-- Indexes for table `history_descs`
--
ALTER TABLE `history_descs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_descs_id_history_foreign` (`id_history`);

--
-- Indexes for table `history_sop`
--
ALTER TABLE `history_sop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_sop_id_post_foreign` (`id_post`);

--
-- Indexes for table `matrix_tips`
--
ALTER TABLE `matrix_tips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matrix_tips_id_post_foreign` (`id_post`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id_post_foreign` (`id_post`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pdf_desc`
--
ALTER TABLE `pdf_desc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pdf_desc_id_pdf_foreign` (`id_pdf`);

--
-- Indexes for table `pdf_pages`
--
ALTER TABLE `pdf_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pdf_pages_id_pdf_foreign` (`id_pdf`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_id_dept_foreign` (`id_dept`);

--
-- Indexes for table `post_user_rels`
--
ALTER TABLE `post_user_rels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_user_rels_id_post_foreign` (`id_post`),
  ADD KEY `post_user_rels_id_user_foreign` (`id_user`);

--
-- Indexes for table `privilage_modules`
--
ALTER TABLE `privilage_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `rule_logins`
--
ALTER TABLE `rule_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sop_pdf`
--
ALTER TABLE `sop_pdf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sop_pdf_id_post_foreign` (`id_post`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `depts`
--
ALTER TABLE `depts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `dept_privilages`
--
ALTER TABLE `dept_privilages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `dept_user`
--
ALTER TABLE `dept_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hc_corners`
--
ALTER TABLE `hc_corners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_descs`
--
ALTER TABLE `history_descs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_sop`
--
ALTER TABLE `history_sop`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `matrix_tips`
--
ALTER TABLE `matrix_tips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pdf_desc`
--
ALTER TABLE `pdf_desc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pdf_pages`
--
ALTER TABLE `pdf_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_user_rels`
--
ALTER TABLE `post_user_rels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `privilage_modules`
--
ALTER TABLE `privilage_modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rule_logins`
--
ALTER TABLE `rule_logins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sop_pdf`
--
ALTER TABLE `sop_pdf`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcements`
--
ALTER TABLE `announcements`
  ADD CONSTRAINT `announcements_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `dept_privilages`
--
ALTER TABLE `dept_privilages`
  ADD CONSTRAINT `dept_privilages_id_dept_foreign` FOREIGN KEY (`id_dept`) REFERENCES `depts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dept_privilages_id_privilage_foreign` FOREIGN KEY (`id_privilage`) REFERENCES `privilage_modules` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `dept_user`
--
ALTER TABLE `dept_user`
  ADD CONSTRAINT `dept_user_dept_id_foreign` FOREIGN KEY (`dept_id`) REFERENCES `depts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dept_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hc_corners`
--
ALTER TABLE `hc_corners`
  ADD CONSTRAINT `hc_corners_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `history_descs`
--
ALTER TABLE `history_descs`
  ADD CONSTRAINT `history_descs_id_history_foreign` FOREIGN KEY (`id_history`) REFERENCES `history_sop` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `history_sop`
--
ALTER TABLE `history_sop`
  ADD CONSTRAINT `history_sop_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrix_tips`
--
ALTER TABLE `matrix_tips`
  ADD CONSTRAINT `matrix_tips_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pdf_desc`
--
ALTER TABLE `pdf_desc`
  ADD CONSTRAINT `pdf_desc_id_pdf_foreign` FOREIGN KEY (`id_pdf`) REFERENCES `sop_pdf` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pdf_pages`
--
ALTER TABLE `pdf_pages`
  ADD CONSTRAINT `pdf_pages_id_pdf_foreign` FOREIGN KEY (`id_pdf`) REFERENCES `sop_pdf` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_id_dept_foreign` FOREIGN KEY (`id_dept`) REFERENCES `depts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_user_rels`
--
ALTER TABLE `post_user_rels`
  ADD CONSTRAINT `post_user_rels_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_user_rels_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sop_pdf`
--
ALTER TABLE `sop_pdf`
  ADD CONSTRAINT `sop_pdf_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
